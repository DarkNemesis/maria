﻿module MarioSis {
    export class GameDataReader {

        private static _instance: GameDataReader;
        mGame: Phaser.Game;

        constructor() {

        }

        public static get Instance() {
            return this._instance || (this._instance = new this());
        }



        public Init(game: Phaser.Game) {
            this.mGame = game;
        }


        mLvlInfos: Array<LevelInfo> = [];

		public ReadLvlInfo(level: number)
		{
			this.mLvlInfos = [];
            var str = this.mGame.cache.getJSON('GameSetting').levelDatas;
            let grpCnt = str[level].groupDatas.length;

            for (var i = 0; i < grpCnt; i++) {

                var cTime = str[level].groupDatas[i].CoolDownTime;
                var cnt = str[level].groupDatas[i].Cnt;
                var type = str[level].groupDatas[i].EnemyIdx;
                let info = new LevelInfo(cTime, cnt, type);
                this.mLvlInfos.push(info);
            }
            return this.mLvlInfos;
        }


       
    }

    export class LevelInfo {

        mCoolDownTime: number;
        mEnemyCnt: number;
        mEnemyType: number
        mIdx:number;
        constructor(cdTime: number, enemyCnt: number,enemyType:number) {
            this.mCoolDownTime = cdTime;
            this.mEnemyCnt = enemyCnt;
            this.mEnemyType = enemyType;
            this.mIdx = 0;
        }

    }


}