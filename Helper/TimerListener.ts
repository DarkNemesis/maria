﻿module MarioSis {
    export class TimerListener {

        private static _instance: TimerListener;
        mGame: Phaser.Game;

        constructor() {

        }

        public static get Instance() {
            return this._instance || (this._instance = new this());
        }


        public InitTimer(game: Phaser.Game) {
            this.mGame = game;
        }


        public Add(miSecond: number, fn: (sender: void) => void) {
            let t = this.mGame.time.create(false);
            t.loop(miSecond, fn, this);
            t.start();
        }
    }

}
