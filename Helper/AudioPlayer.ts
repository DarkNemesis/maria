﻿module MarioSis {
    export class AudioPlayer {

        private static _instance: AudioPlayer;
        mGame: Phaser.Game;
        BGM: Phaser.Sound;

        constructor() {

        }

        public static get Instance() {
            return this._instance || (this._instance = new this());
        }

        public Init(game: Phaser.Game) {
            this.mGame = game;
        }


        playAudio(name: string) {

            let music = this.mGame.add.audio(name, 1, false);
            music.play();

        }

        playBGM(isBoss:boolean) {
            if (isBoss) {
                if (this.BGM != undefined)
                    this.BGM.stop();
                this.BGM = this.mGame.add.audio("BossBGM", 1, true);
            } else {
                if(this.BGM!=undefined)
                this.BGM.stop();
             this.BGM = this.mGame.add.audio("BGM", 1, true);
            }
            this.BGM.play();
        }



        stopBGM() {
            if(this.BGM != undefined)
            this.BGM.stop();
        }
    }

    export class AudioType {

        static PLAYER_DEATH = "PlayerDeath";
        static SHOOT = "ShootSfx";
       static JUMP = "Jump";
       static PICK_UP = "PickKey";
       static MON_DEATH = "MonDeath";
       static BlockHit = "BlockHit";
       static BGM = "BGM";
       static BossBGM = "BossBGM";
    }
}