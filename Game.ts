﻿module MarioSis {
    export class Game extends Phaser.Game {
		constructor()
		{
			super(1920, 1080, Phaser.AUTO, 'content', null);
			
            this.state.add('Boot', Boot, false);
            this.state.add('Preloader', Preloader, false);
		    this.state.add('VedioIntro', VedioPlay, false);
			this.state.add('MainMenu', MainMenu, false);
			this.state.add('TutorialScreen', Tutorial, false);
			this.state.add('LevelMenu', LevelMenu, false);
			this.state.add('GameWorld', GameWorld, false);
			this.state.add('GameOver', GameOver, false);
            this.state.add('CreditList', CreditList, false);
            this.state.start('Boot');
        }
    }

    window.onload = () => {
		var game = new MarioSis.Game();

		addEventListener("keypress", function ()
		{
			var
				el = document.documentElement
				, rfs =
					el.requestFullscreen
					|| el.webkitRequestFullScreen
				;
			rfs.call(el);
		});
    };
} 