﻿module MarioSis
{
	export enum MonsterType
	{
		GHOST, BULLET, PUMPKIN, PEACH
	}

	export enum Direction 
	{
		LEFT,
		RIGHT
	}

	export class Monster extends Phaser.Sprite
	{
		mGameWorld: GameWorld;
		mGame: Phaser.Game;
        mLvlGrp:number;
		mDirection: Direction;
		mSpeed: number;

		mFlipTimer: Phaser.TimerEvent;
		mFlipCueTimer: Phaser.TimerEvent;
		mFlipCueTween: Phaser.Tween;

		mKey: Key;
		mType: MonsterType;

		mGlowBody: Phaser.Rectangle;

		//True if the monster is unconscious
		mFlipped: boolean = false;

		isStandingOnGround: boolean;

        constructor(world: GameWorld, x: number, y: number, d: number, sprite: string,gid: number) 
		{
			super(world.game, x, y, sprite, 0);
		    this.anchor.setTo(0.5, 1);
			this.mDirection = (d == -1) ? Direction.LEFT : Direction.RIGHT;
            this.mLvlGrp = gid;
			this.mGame = world.game;
			this.mGameWorld = world;
			world.game.add.existing(this);
		    world.game.physics.arcade.enable(this);
			
			this.mSpeed = 100;
			
			this.body.drag = new Phaser.Point(200, 100);
			this.body.friction = new Phaser.Point(0, 0);
			this.body.gravity.y = 1000;
			
			this.body.collideWorldBounds = true;		
        }
		
		remove()
		{
			if (this.mFlipTimer)
				this.mGameWorld.game.time.events.remove(this.mFlipTimer);
		    // Special Death Logic

            this.mGameWorld.mMonsterManager.updateMonNum(this.mLvlGrp);

            if (this.mType == MonsterType.GHOST)
            {
				let x = this.body.x;
				let y = this.body.y;
				let point = new Phaser.Point(x, y);

                TimerListener.Instance.Add(2000, () =>
				{
					//this.mGameWorld.mMonsterManager.createGhostMon(x, y);
				});
			}
			if (this.mType == MonsterType.PEACH)
			{
				this.mGameWorld.mKeysCollected = 1;
			}

			this.destroy();
		}

		wrap()
		{
			//Check if the body is escaping the left boundary of the World
			if (this.body.x + this.body.width - 5 < this.mGame.world.bounds.left)
			{
				//If the body is on ground, wrap to the top
				if (this.body.blocked.down == true)
				{
					this.mSpeed = Math.min(400, this.mSpeed + 30);
					this.position.y = this.mGame.world.bounds.top;
					this.position.x += 10;
					this.changeDirectionTo(Direction.RIGHT);
				}
				//Otherwise, wrap to the side
				else
					this.position.x = this.mGame.world.bounds.right + this.body.width / 2 - 15;
			}
			//Check if the body is escaping the right boundary of the World
			else if (this.body.x + 5 > this.mGame.world.bounds.right)
			{
				//If the body is on ground, wrap to the top
				if (this.body.blocked.down == true)
				{
					this.mSpeed = Math.min(400, this.mSpeed + 30);
					this.position.y = this.mGame.world.bounds.top;
					this.position.x -= 10;
					this.changeDirectionTo(Direction.LEFT);
				}
				//Otherwise, wrap to the side
				else
					this.position.x = this.mGame.world.bounds.left - this.body.width / 2 + 15;
			}
		}

		update()
		{
			if (this.body.touching.down)
				this.body.velocity.y = 0;

			if (this.mFlipped)
			{
				this.animations.play('Flipped');
			}
			else if (this.mDirection == Direction.LEFT)
			{
				this.body.velocity.x = -this.mSpeed;
				if (this.mKey)
					this.animations.play('GlowLeft');
				else
					this.animations.play('WalkLeft');
			}
			else if (this.mDirection == Direction.RIGHT)
			{
				this.body.velocity.x = this.mSpeed;
				if (this.mKey)
					this.animations.play('GlowRight');
				else
					this.animations.play('WalkRight');
			}
			this.wrap();
			this.isStandingOnGround = false;
		}

		changeDirectionTo(direction: Direction)
		{
			//If we are already facing in the same direction, or if we are flipped, don't do anything'
			if (this.mDirection == direction || (this.mFlipped == true))
				return;
			//Otherwise, change the direction
			else 
			{
				this.mDirection = direction;
			}
		}

		grabKey(key: Key)
		{
			this.mKey = key;

			if (this.mDirection == Direction.RIGHT)
			{
				this.animations.play('GlowRight');
			}
				
			else if (this.mDirection == Direction.LEFT)
			{
				this.animations.play('GlowLeft');
			}

			this.anchor.x = 0.5;			
			this.anchor.y = (this.mGlowBody.height + this.mGlowBody.top) / this.animations.currentFrame.height;
			this.body.setSize(this.mGlowBody.width, this.mGlowBody.height, this.mGlowBody.left, this.mGlowBody.top);					
		}

		flip()
		{
			//Makes the monster unconscious
			this.mFlipped = true;
			this.mFlipTimer = this.mGame.time.events.add(15 * Phaser.Timer.SECOND, this.unflip, this);
			this.mFlipCueTimer = this.mGame.time.events.add(10 * Phaser.Timer.SECOND, this.unflipCue, this);

			if (this.mKey)
			{
				if (!this.mDirection)
					var a = this.right + 5;
				else
					var a = this.left - 5;
				this.mKey.popOut(a, this.top, this.mDirection);
				this.mGameWorld.mKeyManager.mKeys.add(this.mKey);
				this.mKey = null;
				this.animations.play('Flipped');
				this.anchor.x = 0.5;
				this.anchor.y = 1;
				this.body.setSize(this.mGlowBody.width, this.mGlowBody.height, 0, 0);
			}
		}

		unflipCue()
		{
			this.alpha = 0;
			this.mFlipCueTween = this.mGameWorld.game.add.tween(this);
			this.mFlipCueTween.to({ alpha: 1 }, 500, Phaser.Easing.Linear.None);
			this.mFlipCueTween.loop(true);
			this.mFlipCueTween.start();
		}

		unflip()
		{
			//Makes the monster conscious
		    this.alpha = 1;
			this.mGameWorld.game.tweens.remove(this.mFlipCueTween);
			this.mFlipped = false;
			this.mSpeed = Math.min(400, this.mSpeed + 30);
		}
		
		bounce()
		{
			this.body.velocity.y = -500;
			this.body.velocity.x = (2 * this.mSpeed * (this.mDirection - 1) + this.mSpeed) * 2;
			this.body.touching.down = false;
			if (this.mFlipped)
			{
				this.unflip();
				this.mGame.time.events.remove(this.mFlipTimer);
				this.mGame.time.events.remove(this.mFlipCueTimer);
			}
			else
			{
				this.flip();
			}
		}
	}
	
	export class Peach extends Monster
	{
		mLife: number = 10;

		constructor(world: GameWorld, x: number, y: number, d: number,gid:number)
		{
			super(world, x, y, d, 'Peach',gid);
			this.mType = MonsterType.PEACH;
			
			this.animations.add('WalkLeft', [0, 1, 2], 1, true);
			this.animations.add('WalkRight', [3, 4, 5], 1, true);
			this.animations.add('Flipped', [1], 1, true);

			this.mSpeed = 220;
			
			this.mFlipTimer = this.mGame.time.events.add(20 * Phaser.Timer.SECOND, this.teleport, this);
		}

		update()
		{
			if (this.body.touching.down)
				this.body.velocity.y = 0;

			if (this.mFlipped)
			{
				this.animations.play('Flipped');
			}
			else if (this.mDirection == Direction.LEFT && this.mSpeed != 0)
			{
				this.body.velocity.x = -this.mSpeed;
				this.animations.play('WalkLeft');
			}
			else if (this.mDirection == Direction.RIGHT && this.mSpeed != 0)
			{
				this.body.velocity.x = this.mSpeed;
				this.animations.play('WalkRight');
			}
			this.wrap();
			this.isStandingOnGround = false;
		}

		wrap()
		{
			//Check if the body is escaping the left boundary of the World
			if (this.body.position.x + this.body.width - 5 < this.mGame.world.bounds.left)
			{
				this.position.x = this.mGame.world.bounds.right + this.getBounds().width / 2 - 15;
			}
			//Check if the body is escaping the right boundary of the World
			else if (this.body.position.x + 5 > this.mGame.world.bounds.right)
			{
				this.position.x = this.mGame.world.bounds.left - this.getBounds().width / 2 + 15;
			}
		}

		teleport()
		{
			var tween1 = this.mGameWorld.game.add.tween(this).to({ alpha: 0 }, 1500, Phaser.Easing.Linear.None);
			var tween2 = this.mGameWorld.game.add.tween(this).to({ x: 0, y: 0 }, 1, Phaser.Easing.Linear.None);
			var tween3 = this.mGameWorld.game.add.tween(this).to({ alpha: 1 }, 1500, Phaser.Easing.Linear.None);

			var a = this.mSpeed;
			this.mSpeed = 0;

			tween1.chain(tween2);
			tween2.chain(tween3);
			tween3.onComplete.add(function () { this.mSpeed = Math.min(600, a + 50);}, this);
			tween1.start();

			this.mFlipTimer = this.mGame.time.events.add(25 * Phaser.Timer.SECOND, this.teleport, this);
		}

		flip()
		{
			//Makes the monster unconscious
			this.mLife--;
			if (this.mLife <= 0)
			{
				this.mSpeed = 0;
				this.animations.play('Flipped');
				this.mGame.time.events.remove(this.mFlipTimer);
			}
				
		}
	}
	
	export class Pumpkin extends Monster
	{
        constructor(world: GameWorld, x: number, y: number, d: number,gid: number)
		{
			super(world, x, y, d, MonsterSpriteName.PUMPKIN,gid);

			this.mType = MonsterType.PUMPKIN;

			this.animations.add('WalkLeft', [9, 7, 10, 7], 10, true);
			this.animations.add('WalkRight', [8, 11, 8, 12], 10, true);
			this.animations.add('Flipped', [6], 1, true);
			this.animations.add('GlowRight', [1, 5, 2, 5], 10, true);
			this.animations.add('GlowLeft', [0, 4, 3, 4], 10, true);

			this.mGlowBody = new Phaser.Rectangle(46, 25, 65, 76);
			this.body.setSize(65, 76, 0, 0);
		}

	}

	export class Ghost extends Monster
	{
        constructor(world: GameWorld, x: number, y: number, d: number,gid: number)
        {
			super(world, x, y, d, MonsterSpriteName.GHOST,gid);

			this.mType = MonsterType.GHOST;
			
			this.animations.add('WalkLeft', [0], 1, true);
			this.animations.add('WalkRight', [1], 1, true);
			this.animations.add('Flipped', [2], 1, true);
			this.animations.add('GlowRight', [3], 1, true);
			this.animations.add('GlowLeft', [4], 1, true);

			this.mGlowBody = new Phaser.Rectangle(61, 26, 44, 69);
            this.body.setSize(44, 69, 0, 0);
            this.body.position.x = world.game.rnd.integer() % 1000;
            this.position.x = this.body.position.x;
        }
	}

	export class CandyCorn extends Monster
	{
		constructor(world: GameWorld, x: number, y: number, d: number,gid:number)
		{
			super(world, x, y, d, MonsterSpriteName.BULLET,gid);

			this.mType = MonsterType.BULLET;

			this.animations.add('WalkLeft', [1], 1, true);
			this.animations.add('WalkRight', [0], 1, true);
			this.animations.add('GlowRight', [2], 1, true);
			this.animations.add('GlowLeft', [3], 1, true);

			this.mGlowBody = new Phaser.Rectangle(38, 36, 128, 72);
			this.body.setSize(128, 72, 0, 0);

			this.body.gravity.y = 0;
		}

		update(): void
		{
			if (this.mDirection == Direction.LEFT && this.mSpeed != 0)
			{
				this.body.velocity.x = -this.mSpeed;
				if (this.mKey)
					this.animations.play('GlowLeft');
				else
					this.animations.play('WalkLeft');
			}
			else if (this.mDirection == Direction.RIGHT && this.mSpeed != 0)
			{
				this.body.velocity.x = this.mSpeed;
				if (this.mKey)
					this.animations.play('GlowRight');
				else
					this.animations.play('WalkRight');
			}	

			this.wrap();
		}

		wrap(): void
		{
			//Check if the body is escaping the left boundary of the World
			if (this.body.position.x < this.mGame.world.bounds.left)
			{
				this.changeDirectionTo(Direction.RIGHT);
				this.body.position.x = this.mGame.world.bounds.left;
			}
			//Check if the body is escaping the right boundary of the World
			else if (this.body.position.x + this.body.width > this.mGame.world.bounds.right)
			{
				this.changeDirectionTo(Direction.LEFT);
				this.body.position.x = this.mGame.world.bounds.right - this.body.width;
			}
		}
	}
}