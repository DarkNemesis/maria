﻿module MarioSis
{
	export class SpawnPoint
	{
		x: number;
		y: number;
		direction: number;
		constructor(x, y, direction)
		{
			this.x = x;
			this.y = y;
			this.direction = direction;
		}
	}
    export class MonsterManager {

        mMonsters: Array<Monster> = [];
        mCached: Array<Monster> = [];
        mWorld: GameWorld;



		SpawnPoints: Array<SpawnPoint> = [];
        total: number;
        spawnedCnt: Array<number>=[];
		constructor(world: GameWorld)
		{
			this.mWorld = world;
            this.total = 0;
			for (var i = 0; i < world.mLevelInfo.layers[1].objects.length; i++)
			{
				var data = world.mLevelInfo.layers[1].objects[i];
				this.SpawnPoints.push(new SpawnPoint(data.x, data.y, data.properties.Direction));
			}           
		    GameDataReader.Instance.Init(this.mWorld.game);
			this.startSpawn(this.mWorld.mLevel-1);
		}


        startSpawn(level: number) {
            this.spawnedCnt=[];
            var infos=  GameDataReader.Instance.ReadLvlInfo(level);
            for (var i = 0; i < infos.length; i++) {
                let info = infos[i];
                info.mIdx = i;
                this.spawnedCnt.push(0);
                this.createMonster(info);
                TimerListener.Instance.Add(info.mCoolDownTime * 1000,
                    () => {
                        this.createMonster(info);
                    });

            }
        }

        //Add More Types
        createMonster(info:LevelInfo) {
            if (this.spawnedCnt[info.mIdx] < info.mEnemyCnt) {
                let idx = this.spawnedCnt[info.mIdx] % 2;
                let m = this.enemyFactory(info.mEnemyType, this.SpawnPoints[idx], info.mIdx);
                 this.mMonsters.push(m);
                this.spawnedCnt[info.mIdx]++;

            }
        }

		createGhostMon(position: SpawnPoint) {
          //  let m = this.enemyFactory(MonsterType.GHOST, position);
            //this.mMonsters.push(m);
        }


		enemyFactory(type: number, position: SpawnPoint,lvlGrpIdx:number)
		{
            let mon;
            switch (type) {
                case 0:
                    //should be pumpkin
                    mon = new Pumpkin (this.mWorld, position.x, position.y, position.direction, lvlGrpIdx);
                    break;
                case 1:
                    mon = new Ghost(this.mWorld, position.x, position.y, position.direction, lvlGrpIdx);
                    break;
                case 2:
                    mon = new CandyCorn(this.mWorld, position.x, position.y, position.direction, lvlGrpIdx);
                    break;
                case 3:
                    mon = new Peach(this.mWorld, position.x, position.y, position.direction, lvlGrpIdx);
                    break;
            default:
            }
            return mon;

        }

        update() {
        }


        removeMonster(key: Monster) {
            var index = -1;
 
                index = this.mMonsters.indexOf(key, 0);
                if (index > -1) {
                    this.mMonsters.splice(index, 1);
               
                }
           
        }

        updateMonNum(gid:number) {

            if (this.spawnedCnt != undefined && this.spawnedCnt.length >= gid) {
                this.spawnedCnt[gid]--;
            }
           

        }
        }
    }
