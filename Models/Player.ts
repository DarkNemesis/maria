﻿module MarioSis
{
	export class Player extends Phaser.Sprite
	{        
		mWorld: GameWorld;
		mInvincibleTween: Phaser.Tween;
		mIsInvincible: boolean = true;
      
        constructor(gameWorld: GameWorld)
		{
			super(gameWorld.game, gameWorld.game.world.bounds.width / 2, gameWorld.game.world.bounds.bottom - 100, 'Maria');
			this.mWorld = gameWorld;	
			
			this.anchor.setTo(0.5, 0);			
            this.animations.add('Walking', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], 12, true);   
			
            
			//Set up Physics
			this.mWorld.game.physics.arcade.enable(this);
			this.body.gravity.y = 2000;
			this.body.collideWorldBounds = true;	
						
			this.body.x = 10;
			this.body.y = 1;
			this.body.width = 40;
			this.body.height = 102;

			this.mInvincibleTween = this.mWorld.game.add.tween(this).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true, 0, 2);
			this.mInvincibleTween.onComplete.add(function () { this.alpha = 1; this.mIsInvincible = false }, this);			
		}

		wrap()
		{
			if (this.body.position.x + this.body.width - 5 < this.mWorld.game.world.bounds.left)
			{
				this.position.x = this.mWorld.game.world.bounds.right + this.getBounds().width / 2 - 15;
			}
			else if (this.body.position.x + 5 > this.mWorld.game.world.bounds.right)
			{
				this.position.x = this.mWorld.game.world.bounds.left - this.getBounds().width / 2 + 15;
			}
		}

		update()
		{			
            this.updateInput();
			this.wrap();
        }

		updateInput()
		{
			this.body.velocity.x = 0;
			
			if (this.mWorld.game.input.keyboard.isDown(Phaser.Keyboard.LEFT))
			{				
				this.animations.play('Walking');
				if (this.scale.x == -1)
				{
					this.scale.x = 1;
					this.body.position.x += this.body.width;
				}
				else
				{
					this.body.velocity.x = -250;
				}
			}
			else if (this.mWorld.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT))
			{				
				this.animations.play('Walking');
				if (this.scale.x == 1)
				{
					this.scale.x = -1;
					this.body.position.x -= this.body.width;
				}
				else
				{
					this.body.velocity.x = 250;
				}
			}
			else
			{
                this.animations.frame = 13;
			}
		  
			if (this.mWorld.game.input.keyboard.isDown(Phaser.Keyboard.UP) && this.body.blocked.down)
			{
                this.body.velocity.y = -1000;
                AudioPlayer.Instance.playAudio(AudioType.JUMP);
            }

        }


	}
}