﻿module MarioSis
{
	export class Key extends Phaser.Sprite
	{
		mWorld: GameWorld;
		constructor(x: number, y: number, gameWorld: GameWorld)
		{
			super(gameWorld.game, x, y, 'Candy', 0);
			this.mWorld = gameWorld;
						
			//Set up Physics
			this.mWorld.game.physics.arcade.enable(this);
						
			this.body.friction.x = 1000;
			this.body.friction.y = 1000;
			this.body.gravity.y = 1500;

			this.body.collideWorldBounds = true;
		}

		render()
		{			
		}

		update()
		{
			if (this.body.touching.down)
			{
				this.body.velocity.y = 0;
			}	
			this.wrap();
		}

		wrap()
		{
			//Check if the body is escaping the left boundary of the World
			if (this.body.position.x + this.body.width - 5 < this.mWorld.game.world.bounds.left)
			{
				this.position.x = this.mWorld.game.world.bounds.right + this.getBounds().width / 2 - 15;
			}
			//Check if the body is escaping the right boundary of the World
			else if (this.body.position.x + 5 > this.mWorld.game.world.bounds.right)
			{
				this.position.x = this.mWorld.game.world.bounds.left - this.getBounds().width / 2 + 15;
			}
		}

		popOut(x: number, y: number, d: Direction)
		{
			this.position.x = x;
			this.position.y = y;

			var offset = (d - 1) * 600 + 300;
			this.mWorld.game.physics.arcade.moveToXY(this, x + offset, y, 200);
			//this.body.velocity.y = -750;
			//this.body.velocity.x = d * 750;
		}
	}	
}      