﻿module MarioSis
{
	export class PlayerManager
	{
		mLife: number;
		mGameWorld: GameWorld;
        mPlayer: Player;
	    mWeapon: WeaponManager;
        mFireKey: Phaser.Key;


		constructor(gameWorld: GameWorld)
		{
			this.mGameWorld = gameWorld;
			this.mLife = 5;
            this.mFireKey = this.mGameWorld.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
		}

		createNewPlayer(): boolean
		{
			if (this.mLife == 0)
				return false;
			else
			{
                this.mLife--;
                this.mPlayer = new Player(this.mGameWorld);
                this.mWeapon = new WeaponManager(this.mPlayer);
				this.mGameWorld.game.add.existing(this.mPlayer);
				this.mGameWorld.game.camera.follow(this.mPlayer);
			}
		}

		destroyPlayer()
		{
			if (this.mPlayer.mIsInvincible)
				return;

            this.mPlayer.destroy();
		
			this.mPlayer = undefined;

			if (this.mLife > 0)
			{
                AudioPlayer.Instance.playAudio(AudioType.SHOOT);
				this.mGameWorld.game.time.events.add(3 * Phaser.Timer.SECOND, this.createNewPlayer, this);
			} else {
			    AudioPlayer.Instance.playAudio(AudioType.PLAYER_DEATH);
			}
        }


        update() {

            if (this.mFireKey.justUp&&this.mWeapon!=null) {
                this.mWeapon.throwItem();
            }
            this.mWeapon.update();
        }
	}
}