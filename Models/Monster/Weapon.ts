﻿module MarioSis {
  export  class Weapon {
      mSprite: Phaser.Sprite;
      owner: Player;
      spwanX: number;

      constructor(player: Player) {
          this.owner = player;
      }

      spawn() {
          let game = this.owner.mWorld.game;
          let xOffset = -this.owner.scale.x * 30;
          xOffset = xOffset < 0 ? xOffset + 50 : xOffset;
          AudioPlayer.Instance.playAudio(AudioType.SHOOT);
          let knife = game.add.sprite(this.owner.body.x + xOffset, this.owner.body.y+30
           , "Heart");
          this.spwanX = this.owner.body.x + xOffset;
          game.physics.arcade.enable(knife);
          knife.anchor.set(0.5, 0.5);
          knife.scale.x = .85;
          knife.scale.y = .85;
          knife.body.velocity.x = -this.owner.scale.x*175;
          knife.body.mass = 0;
          knife.body.setSize(32, 32);
          this.mSprite = knife;
      }


      die() {

          if (Math.abs(this.spwanX - this.mSprite.x) > 200) {
              this.mSprite.destroy();
              this.owner.mWorld.mPlayerManager.mWeapon.removeWeapon(this.mSprite);
              this.owner.mWorld.mPlayerManager.mWeapon.removeWeaponWrapper(this);
          }

      }


      update() {
          this.die();
      }



    }
}