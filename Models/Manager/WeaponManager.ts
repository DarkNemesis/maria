﻿module MarioSis {
    export class WeaponManager {

        owner: Player;
        CD_SPAN=30000;
        cooldown: number;
        cooldownT: number;
        mWeapons: Array<Phaser.Sprite> = [];
        mWeaponsWrpper: Array<Weapon> = [];
        cached: Array<Weapon>=[];

        constructor(player: Player) {
            this.owner = player;
            this.cooldown = 0;
            this.cooldownT = 30000;
            TimerListener.Instance.Add(1000, () => {
                this.counterTime();
            });
        }


        spwanFactory() {
            let w = new Weapon(this.owner);
            w.spawn();
            this.mWeaponsWrpper.push(w);
            return w.mSprite;
        }


        //Add WeaponType
        throwItem() {
            if (this.owner != null && this.owner.body != null &&this.cooldown <= 0) {
                this.mWeapons.push(this.spwanFactory());
                this.startCoolDown();
            }

        }


        startCoolDown() {
             if (this.cooldown <= 0) {
                this.cooldown = this.CD_SPAN;
                TimerListener.Instance.Add(this.CD_SPAN,
                    () => {
                        this.cooldown = 0;
                    });
            }
             this.cooldownT = 0;
        }


        counterTime() {
            if (this.cooldownT < 30000) {
                this.cooldownT += 1000;
            };
        
        }


        getPercent() {

            return Phaser.Math.roundTo((this.cooldownT) * 100 / this.CD_SPAN);
        }

        removeWeapon(key: Phaser.Sprite) {
            var index = this.mWeapons.indexOf(key, 0);
            if (index > -1) {
                this.mWeapons.splice(index, 1);

            }
        }
        removeWeaponWrapper(key: Weapon) {
            var index = this.mWeaponsWrpper.indexOf(key, 0);
            if (index > -1) {
                this.mWeaponsWrpper.splice(index, 1);

            }
        }


        update() {
            for (var i = 0; i < this.mWeaponsWrpper.length; i++) {
              this.mWeaponsWrpper[i].update();
            }

            
         
        }
    }


}