﻿module MarioSis
{
	export class KeyManager
	{
		mMaxKeys: number;
		mKeyCount: number;
		mGameWorld: GameWorld;
		mKeys: Phaser.Group;
		mHealthyList: Phaser.ArraySet;
		mSpawnTimer: Phaser.TimerEvent;
		mSpawnRate: number

		constructor(gameWorld: GameWorld)
		{
			this.mGameWorld = gameWorld;
			this.mMaxKeys = this.mGameWorld.mLevelInfo.properties.MaxKey;
			this.mKeyCount = 0;
			this.mKeys = this.mGameWorld.game.add.group();

			this.mSpawnRate = 4;

			this.mHealthyList = this.mGameWorld.mPlatform.mTiles.filter(function (child, index, children)
			{
				var a = child.mPlatform.mTiles.getClosestTo(new Phaser.Point(child.position.x, child.position.y - 36));
				return (a.position.x != child.position.x || a.position.y != child.position.y - 36);

			}, true);
		}

		createNewKey()
		{
			if (this.mKeyCount < 3 && this.mMaxKeys > 0)
			{
				this.mMaxKeys--;
				this.mKeyCount++;
				do
				{
					var r = this.mGameWorld.game.rnd.integerInRange(0, this.mHealthyList.total - 1);
					var x = this.mHealthyList.list[r].position.x;
					var y = this.mHealthyList.list[r].position.y - 36;
					var closest = this.mKeys.getClosestTo(new Phaser.Point(x, y));
				} while (closest && closest.position.x == x && closest.position.y - y <= 5);

				this.mKeys.add(new Key(x, y - 5, this.mGameWorld));

				if (this.mMaxKeys > 0)
					this.mSpawnTimer = this.mGameWorld.game.time.events.add(this.mSpawnRate * Phaser.Timer.SECOND, this.createNewKey, this);
			}
			else
			{
				this.mSpawnTimer = this.mGameWorld.game.time.events.add(1 * Phaser.Timer.SECOND, this.createNewKey, this);
			}				
		}
	}
}