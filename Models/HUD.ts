﻿module MarioSis
{
	export class HUD
	{
		mGameWorld: GameWorld;

		mHUD: Phaser.Sprite;

		mLife: Phaser.Sprite;
		mCandy: Phaser.Sprite;
		mWeapon: Phaser.Sprite;

		mHUDLife:	Phaser.Text;
		mHUDKey: Phaser.Text;
		mHUDWeapon: Phaser.Text;
		
		constructor(gameWorld: GameWorld)
		{
			this.mGameWorld = gameWorld;

			this.mHUD = this.mGameWorld.game.add.sprite(0, 972, 'Ground');
			this.mHUD.width = 1920;
			this.mHUD.height = 36 * 3;

			var style = { font: "bold 32px Gothic", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };

			this.mLife = this.mGameWorld.game.add.sprite(15, 987, 'HUDLife');
			if (this.mGameWorld.mLevel == 3)
				this.mCandy = this.mGameWorld.game.add.sprite(650, 987, 'HUDPeach');
			else
				this.mCandy = this.mGameWorld.game.add.sprite(650, 987, 'HUDCandy');
			this.mWeapon = this.mGameWorld.game.add.sprite(1285, 987, 'HUDWeapon');

			this.mHUDLife = this.mGameWorld.game.add.text(95, 1017, "", style);
			this.mHUDKey = this.mGameWorld.game.add.text(730, 1017, "", style);
			this.mHUDWeapon = this.mGameWorld.game.add.text(1365, 1017, "", style);

			this.mHUDLife.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
			this.mHUDKey.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);	
			this.mHUDWeapon.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);	

			this.mHUDLife.addColor('#ffffff', 0);
			this.mHUDKey.addColor('#ffffff', 0);
			this.mHUDWeapon.addColor('#ffffff', 0);
		}

		update()
		{
			this.mHUDLife.text = "x" + this.mGameWorld.mPlayerManager.mLife.toString();
			this.mHUDKey.text = this.mGameWorld.mKeysCollected.toString() + "/" + this.mGameWorld.mLevelInfo.properties.KeyCount.toString();
			this.mHUDWeapon.text = this.mGameWorld.mPlayerManager.mWeapon.getPercent().toString() + "%";
		}
	}
}