﻿module MarioSis
{
	export class Platform
	{
		mWorld: GameWorld;			
        mTiles: Phaser.Group;
	    mWeapons: Phaser.Group;

		loadLevel()
		{
			var mLevelData = this.mWorld.mLevelInfo;
					
			for (var i = 0; i < mLevelData.layers[0].data.length; i++)
			{
				var e = mLevelData.layers[0].data[i];
				if (e == 0)
					continue;
				else
				{
					var y = Math.floor(i / mLevelData.layers[0].width);
					var x = i % mLevelData.layers[0].width;
					var w = mLevelData.tilewidth;
					var h = mLevelData.tileheight;
					this.mTiles.add(new Tile(this, x * w, y * h, e - 1));					
				}
			}		
		}

		constructor(world: GameWorld)
		{
			this.mWorld = world;
			this.mTiles = this.mWorld.game.add.group();
			this.mWeapons = this.mWorld.game.add.group();
			this.loadLevel();	
		}		
	}
}