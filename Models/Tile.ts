﻿module MarioSis
{
	export class Tile extends Phaser.Sprite
	{
		mPlatform: Platform;
		mBouncing: boolean = false;
		mVelocity: number = -3;
		constructor(platform: Platform, x: number, y: number, type: number) 
		{
			super(platform.mWorld.game, x, y, 'Tiles', type);

			this.mPlatform = platform;
			this.mPlatform.mWorld.game.physics.arcade.enable(this);
			this.body.immovable = true;
		}

		bounce()
		{
			if (this.mBouncing == false)
			{
				this.mBouncing = true;
				this.mVelocity = -3;				
			}			
		}

		update()
		{
			if (this.mBouncing == true)
			{
				this.position.y += this.mVelocity;
				this.mVelocity += 0.5;

				if (this.mVelocity > 3)
					this.mBouncing = false;
			}			
		}
	}
}