var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var MarioSis;
(function (MarioSis) {
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            var _this = _super.call(this, 1920, 1080, Phaser.AUTO, 'content', null) || this;
            _this.state.add('Boot', MarioSis.Boot, false);
            _this.state.add('Preloader', MarioSis.Preloader, false);
            _this.state.add('VedioIntro', MarioSis.VedioPlay, false);
            _this.state.add('MainMenu', MarioSis.MainMenu, false);
            _this.state.add('TutorialScreen', MarioSis.Tutorial, false);
            _this.state.add('LevelMenu', MarioSis.LevelMenu, false);
            _this.state.add('GameWorld', MarioSis.GameWorld, false);
            _this.state.add('GameOver', MarioSis.GameOver, false);
            _this.state.add('CreditList', MarioSis.CreditList, false);
            _this.state.start('Boot');
            return _this;
        }
        return Game;
    }(Phaser.Game));
    MarioSis.Game = Game;
    window.onload = function () {
        var game = new MarioSis.Game();
        addEventListener("keypress", function () {
            var el = document.documentElement, rfs = el.requestFullscreen
                || el.webkitRequestFullScreen;
            rfs.call(el);
        });
    };
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var AudioPlayer = (function () {
        function AudioPlayer() {
        }
        Object.defineProperty(AudioPlayer, "Instance", {
            get: function () {
                return this._instance || (this._instance = new this());
            },
            enumerable: true,
            configurable: true
        });
        AudioPlayer.prototype.Init = function (game) {
            this.mGame = game;
        };
        AudioPlayer.prototype.playAudio = function (name) {
            var music = this.mGame.add.audio(name, 1, false);
            music.play();
        };
        AudioPlayer.prototype.playBGM = function (isBoss) {
            if (isBoss) {
                if (this.BGM != undefined)
                    this.BGM.stop();
                this.BGM = this.mGame.add.audio("BossBGM", 1, true);
            }
            else {
                if (this.BGM != undefined)
                    this.BGM.stop();
                this.BGM = this.mGame.add.audio("BGM", 1, true);
            }
            this.BGM.play();
        };
        AudioPlayer.prototype.stopBGM = function () {
            if (this.BGM != undefined)
                this.BGM.stop();
        };
        return AudioPlayer;
    }());
    MarioSis.AudioPlayer = AudioPlayer;
    var AudioType = (function () {
        function AudioType() {
        }
        return AudioType;
    }());
    AudioType.PLAYER_DEATH = "PlayerDeath";
    AudioType.SHOOT = "ShootSfx";
    AudioType.JUMP = "Jump";
    AudioType.PICK_UP = "PickKey";
    AudioType.MON_DEATH = "MonDeath";
    AudioType.BlockHit = "BlockHit";
    AudioType.BGM = "BGM";
    AudioType.BossBGM = "BossBGM";
    MarioSis.AudioType = AudioType;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var GameDataReader = (function () {
        function GameDataReader() {
            this.mLvlInfos = [];
        }
        Object.defineProperty(GameDataReader, "Instance", {
            get: function () {
                return this._instance || (this._instance = new this());
            },
            enumerable: true,
            configurable: true
        });
        GameDataReader.prototype.Init = function (game) {
            this.mGame = game;
        };
        GameDataReader.prototype.ReadLvlInfo = function (level) {
            this.mLvlInfos = [];
            var str = this.mGame.cache.getJSON('GameSetting').levelDatas;
            var grpCnt = str[level].groupDatas.length;
            for (var i = 0; i < grpCnt; i++) {
                var cTime = str[level].groupDatas[i].CoolDownTime;
                var cnt = str[level].groupDatas[i].Cnt;
                var type = str[level].groupDatas[i].EnemyIdx;
                var info = new LevelInfo(cTime, cnt, type);
                this.mLvlInfos.push(info);
            }
            return this.mLvlInfos;
        };
        return GameDataReader;
    }());
    MarioSis.GameDataReader = GameDataReader;
    var LevelInfo = (function () {
        function LevelInfo(cdTime, enemyCnt, enemyType) {
            this.mCoolDownTime = cdTime;
            this.mEnemyCnt = enemyCnt;
            this.mEnemyType = enemyType;
            this.mIdx = 0;
        }
        return LevelInfo;
    }());
    MarioSis.LevelInfo = LevelInfo;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var TimerListener = (function () {
        function TimerListener() {
        }
        Object.defineProperty(TimerListener, "Instance", {
            get: function () {
                return this._instance || (this._instance = new this());
            },
            enumerable: true,
            configurable: true
        });
        TimerListener.prototype.InitTimer = function (game) {
            this.mGame = game;
        };
        TimerListener.prototype.Add = function (miSecond, fn) {
            var t = this.mGame.time.create(false);
            t.loop(miSecond, fn, this);
            t.start();
        };
        return TimerListener;
    }());
    MarioSis.TimerListener = TimerListener;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var HUD = (function () {
        function HUD(gameWorld) {
            this.mGameWorld = gameWorld;
            this.mHUD = this.mGameWorld.game.add.sprite(0, 972, 'Ground');
            this.mHUD.width = 1920;
            this.mHUD.height = 36 * 3;
            var style = { font: "bold 32px Gothic", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
            this.mLife = this.mGameWorld.game.add.sprite(15, 987, 'HUDLife');
            if (this.mGameWorld.mLevel == 3)
                this.mCandy = this.mGameWorld.game.add.sprite(650, 987, 'HUDPeach');
            else
                this.mCandy = this.mGameWorld.game.add.sprite(650, 987, 'HUDCandy');
            this.mWeapon = this.mGameWorld.game.add.sprite(1285, 987, 'HUDWeapon');
            this.mHUDLife = this.mGameWorld.game.add.text(95, 1017, "", style);
            this.mHUDKey = this.mGameWorld.game.add.text(730, 1017, "", style);
            this.mHUDWeapon = this.mGameWorld.game.add.text(1365, 1017, "", style);
            this.mHUDLife.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
            this.mHUDKey.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
            this.mHUDWeapon.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
            this.mHUDLife.addColor('#ffffff', 0);
            this.mHUDKey.addColor('#ffffff', 0);
            this.mHUDWeapon.addColor('#ffffff', 0);
        }
        HUD.prototype.update = function () {
            this.mHUDLife.text = "x" + this.mGameWorld.mPlayerManager.mLife.toString();
            this.mHUDKey.text = this.mGameWorld.mKeysCollected.toString() + "/" + this.mGameWorld.mLevelInfo.properties.KeyCount.toString();
            this.mHUDWeapon.text = this.mGameWorld.mPlayerManager.mWeapon.getPercent().toString() + "%";
        };
        return HUD;
    }());
    MarioSis.HUD = HUD;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var Key = (function (_super) {
        __extends(Key, _super);
        function Key(x, y, gameWorld) {
            var _this = _super.call(this, gameWorld.game, x, y, 'Candy', 0) || this;
            _this.mWorld = gameWorld;
            //Set up Physics
            _this.mWorld.game.physics.arcade.enable(_this);
            _this.body.friction.x = 1000;
            _this.body.friction.y = 1000;
            _this.body.gravity.y = 1500;
            _this.body.collideWorldBounds = true;
            return _this;
        }
        Key.prototype.render = function () {
        };
        Key.prototype.update = function () {
            if (this.body.touching.down) {
                this.body.velocity.y = 0;
            }
            this.wrap();
        };
        Key.prototype.wrap = function () {
            //Check if the body is escaping the left boundary of the World
            if (this.body.position.x + this.body.width - 5 < this.mWorld.game.world.bounds.left) {
                this.position.x = this.mWorld.game.world.bounds.right + this.getBounds().width / 2 - 15;
            }
            else if (this.body.position.x + 5 > this.mWorld.game.world.bounds.right) {
                this.position.x = this.mWorld.game.world.bounds.left - this.getBounds().width / 2 + 15;
            }
        };
        Key.prototype.popOut = function (x, y, d) {
            this.position.x = x;
            this.position.y = y;
            var offset = (d - 1) * 600 + 300;
            this.mWorld.game.physics.arcade.moveToXY(this, x + offset, y, 200);
            //this.body.velocity.y = -750;
            //this.body.velocity.x = d * 750;
        };
        return Key;
    }(Phaser.Sprite));
    MarioSis.Key = Key;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var KeyManager = (function () {
        function KeyManager(gameWorld) {
            this.mGameWorld = gameWorld;
            this.mMaxKeys = this.mGameWorld.mLevelInfo.properties.MaxKey;
            this.mKeyCount = 0;
            this.mKeys = this.mGameWorld.game.add.group();
            this.mSpawnRate = 4;
            this.mHealthyList = this.mGameWorld.mPlatform.mTiles.filter(function (child, index, children) {
                var a = child.mPlatform.mTiles.getClosestTo(new Phaser.Point(child.position.x, child.position.y - 36));
                return (a.position.x != child.position.x || a.position.y != child.position.y - 36);
            }, true);
        }
        KeyManager.prototype.createNewKey = function () {
            if (this.mKeyCount < 3 && this.mMaxKeys > 0) {
                this.mMaxKeys--;
                this.mKeyCount++;
                do {
                    var r = this.mGameWorld.game.rnd.integerInRange(0, this.mHealthyList.total - 1);
                    var x = this.mHealthyList.list[r].position.x;
                    var y = this.mHealthyList.list[r].position.y - 36;
                    var closest = this.mKeys.getClosestTo(new Phaser.Point(x, y));
                } while (closest && closest.position.x == x && closest.position.y - y <= 5);
                this.mKeys.add(new MarioSis.Key(x, y - 5, this.mGameWorld));
                if (this.mMaxKeys > 0)
                    this.mSpawnTimer = this.mGameWorld.game.time.events.add(this.mSpawnRate * Phaser.Timer.SECOND, this.createNewKey, this);
            }
            else {
                this.mSpawnTimer = this.mGameWorld.game.time.events.add(1 * Phaser.Timer.SECOND, this.createNewKey, this);
            }
        };
        return KeyManager;
    }());
    MarioSis.KeyManager = KeyManager;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var WeaponManager = (function () {
        function WeaponManager(player) {
            var _this = this;
            this.CD_SPAN = 30000;
            this.mWeapons = [];
            this.mWeaponsWrpper = [];
            this.cached = [];
            this.owner = player;
            this.cooldown = 0;
            this.cooldownT = 30000;
            MarioSis.TimerListener.Instance.Add(1000, function () {
                _this.counterTime();
            });
        }
        WeaponManager.prototype.spwanFactory = function () {
            var w = new MarioSis.Weapon(this.owner);
            w.spawn();
            this.mWeaponsWrpper.push(w);
            return w.mSprite;
        };
        //Add WeaponType
        WeaponManager.prototype.throwItem = function () {
            if (this.owner != null && this.owner.body != null && this.cooldown <= 0) {
                this.mWeapons.push(this.spwanFactory());
                this.startCoolDown();
            }
        };
        WeaponManager.prototype.startCoolDown = function () {
            var _this = this;
            if (this.cooldown <= 0) {
                this.cooldown = this.CD_SPAN;
                MarioSis.TimerListener.Instance.Add(this.CD_SPAN, function () {
                    _this.cooldown = 0;
                });
            }
            this.cooldownT = 0;
        };
        WeaponManager.prototype.counterTime = function () {
            if (this.cooldownT < 30000) {
                this.cooldownT += 1000;
            }
            ;
        };
        WeaponManager.prototype.getPercent = function () {
            return Phaser.Math.roundTo((this.cooldownT) * 100 / this.CD_SPAN);
        };
        WeaponManager.prototype.removeWeapon = function (key) {
            var index = this.mWeapons.indexOf(key, 0);
            if (index > -1) {
                this.mWeapons.splice(index, 1);
            }
        };
        WeaponManager.prototype.removeWeaponWrapper = function (key) {
            var index = this.mWeaponsWrpper.indexOf(key, 0);
            if (index > -1) {
                this.mWeaponsWrpper.splice(index, 1);
            }
        };
        WeaponManager.prototype.update = function () {
            for (var i = 0; i < this.mWeaponsWrpper.length; i++) {
                this.mWeaponsWrpper[i].update();
            }
        };
        return WeaponManager;
    }());
    MarioSis.WeaponManager = WeaponManager;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var MonsterSpriteName = (function () {
        function MonsterSpriteName() {
        }
        return MonsterSpriteName;
    }());
    //add name on it
    MonsterSpriteName.GHOST = 'Ghost';
    MonsterSpriteName.BULLET = 'CandyCorn';
    MonsterSpriteName.PUMPKIN = 'Pumpkin';
    MarioSis.MonsterSpriteName = MonsterSpriteName;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var Weapon = (function () {
        function Weapon(player) {
            this.owner = player;
        }
        Weapon.prototype.spawn = function () {
            var game = this.owner.mWorld.game;
            var xOffset = -this.owner.scale.x * 30;
            xOffset = xOffset < 0 ? xOffset + 50 : xOffset;
            MarioSis.AudioPlayer.Instance.playAudio(MarioSis.AudioType.SHOOT);
            var knife = game.add.sprite(this.owner.body.x + xOffset, this.owner.body.y + 30, "Heart");
            this.spwanX = this.owner.body.x + xOffset;
            game.physics.arcade.enable(knife);
            knife.anchor.set(0.5, 0.5);
            knife.scale.x = .85;
            knife.scale.y = .85;
            knife.body.velocity.x = -this.owner.scale.x * 175;
            knife.body.mass = 0;
            knife.body.setSize(32, 32);
            this.mSprite = knife;
        };
        Weapon.prototype.die = function () {
            if (Math.abs(this.spwanX - this.mSprite.x) > 200) {
                this.mSprite.destroy();
                this.owner.mWorld.mPlayerManager.mWeapon.removeWeapon(this.mSprite);
                this.owner.mWorld.mPlayerManager.mWeapon.removeWeaponWrapper(this);
            }
        };
        Weapon.prototype.update = function () {
            this.die();
        };
        return Weapon;
    }());
    MarioSis.Weapon = Weapon;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var MonsterType;
    (function (MonsterType) {
        MonsterType[MonsterType["GHOST"] = 0] = "GHOST";
        MonsterType[MonsterType["BULLET"] = 1] = "BULLET";
        MonsterType[MonsterType["PUMPKIN"] = 2] = "PUMPKIN";
        MonsterType[MonsterType["PEACH"] = 3] = "PEACH";
    })(MonsterType = MarioSis.MonsterType || (MarioSis.MonsterType = {}));
    var Direction;
    (function (Direction) {
        Direction[Direction["LEFT"] = 0] = "LEFT";
        Direction[Direction["RIGHT"] = 1] = "RIGHT";
    })(Direction = MarioSis.Direction || (MarioSis.Direction = {}));
    var Monster = (function (_super) {
        __extends(Monster, _super);
        function Monster(world, x, y, d, sprite, gid) {
            var _this = _super.call(this, world.game, x, y, sprite, 0) || this;
            //True if the monster is unconscious
            _this.mFlipped = false;
            _this.anchor.setTo(0.5, 1);
            _this.mDirection = (d == -1) ? Direction.LEFT : Direction.RIGHT;
            _this.mLvlGrp = gid;
            _this.mGame = world.game;
            _this.mGameWorld = world;
            world.game.add.existing(_this);
            world.game.physics.arcade.enable(_this);
            _this.mSpeed = 100;
            _this.body.drag = new Phaser.Point(200, 100);
            _this.body.friction = new Phaser.Point(0, 0);
            _this.body.gravity.y = 1000;
            _this.body.collideWorldBounds = true;
            return _this;
        }
        Monster.prototype.remove = function () {
            if (this.mFlipTimer)
                this.mGameWorld.game.time.events.remove(this.mFlipTimer);
            // Special Death Logic
            this.mGameWorld.mMonsterManager.updateMonNum(this.mLvlGrp);
            if (this.mType == MonsterType.GHOST) {
                var x = this.body.x;
                var y = this.body.y;
                var point = new Phaser.Point(x, y);
                MarioSis.TimerListener.Instance.Add(2000, function () {
                    //this.mGameWorld.mMonsterManager.createGhostMon(x, y);
                });
            }
            if (this.mType == MonsterType.PEACH) {
                this.mGameWorld.mKeysCollected = 1;
            }
            this.destroy();
        };
        Monster.prototype.wrap = function () {
            //Check if the body is escaping the left boundary of the World
            if (this.body.x + this.body.width - 5 < this.mGame.world.bounds.left) {
                //If the body is on ground, wrap to the top
                if (this.body.blocked.down == true) {
                    this.mSpeed = Math.min(400, this.mSpeed + 30);
                    this.position.y = this.mGame.world.bounds.top;
                    this.position.x += 10;
                    this.changeDirectionTo(Direction.RIGHT);
                }
                else
                    this.position.x = this.mGame.world.bounds.right + this.body.width / 2 - 15;
            }
            else if (this.body.x + 5 > this.mGame.world.bounds.right) {
                //If the body is on ground, wrap to the top
                if (this.body.blocked.down == true) {
                    this.mSpeed = Math.min(400, this.mSpeed + 30);
                    this.position.y = this.mGame.world.bounds.top;
                    this.position.x -= 10;
                    this.changeDirectionTo(Direction.LEFT);
                }
                else
                    this.position.x = this.mGame.world.bounds.left - this.body.width / 2 + 15;
            }
        };
        Monster.prototype.update = function () {
            if (this.body.touching.down)
                this.body.velocity.y = 0;
            if (this.mFlipped) {
                this.animations.play('Flipped');
            }
            else if (this.mDirection == Direction.LEFT) {
                this.body.velocity.x = -this.mSpeed;
                if (this.mKey)
                    this.animations.play('GlowLeft');
                else
                    this.animations.play('WalkLeft');
            }
            else if (this.mDirection == Direction.RIGHT) {
                this.body.velocity.x = this.mSpeed;
                if (this.mKey)
                    this.animations.play('GlowRight');
                else
                    this.animations.play('WalkRight');
            }
            this.wrap();
            this.isStandingOnGround = false;
        };
        Monster.prototype.changeDirectionTo = function (direction) {
            //If we are already facing in the same direction, or if we are flipped, don't do anything'
            if (this.mDirection == direction || (this.mFlipped == true))
                return;
            else {
                this.mDirection = direction;
            }
        };
        Monster.prototype.grabKey = function (key) {
            this.mKey = key;
            if (this.mDirection == Direction.RIGHT) {
                this.animations.play('GlowRight');
            }
            else if (this.mDirection == Direction.LEFT) {
                this.animations.play('GlowLeft');
            }
            this.anchor.x = 0.5;
            this.anchor.y = (this.mGlowBody.height + this.mGlowBody.top) / this.animations.currentFrame.height;
            this.body.setSize(this.mGlowBody.width, this.mGlowBody.height, this.mGlowBody.left, this.mGlowBody.top);
        };
        Monster.prototype.flip = function () {
            //Makes the monster unconscious
            this.mFlipped = true;
            this.mFlipTimer = this.mGame.time.events.add(15 * Phaser.Timer.SECOND, this.unflip, this);
            this.mFlipCueTimer = this.mGame.time.events.add(10 * Phaser.Timer.SECOND, this.unflipCue, this);
            if (this.mKey) {
                if (!this.mDirection)
                    var a = this.right + 5;
                else
                    var a = this.left - 5;
                this.mKey.popOut(a, this.top, this.mDirection);
                this.mGameWorld.mKeyManager.mKeys.add(this.mKey);
                this.mKey = null;
                this.animations.play('Flipped');
                this.anchor.x = 0.5;
                this.anchor.y = 1;
                this.body.setSize(this.mGlowBody.width, this.mGlowBody.height, 0, 0);
            }
        };
        Monster.prototype.unflipCue = function () {
            this.alpha = 0;
            this.mFlipCueTween = this.mGameWorld.game.add.tween(this);
            this.mFlipCueTween.to({ alpha: 1 }, 500, Phaser.Easing.Linear.None);
            this.mFlipCueTween.loop(true);
            this.mFlipCueTween.start();
        };
        Monster.prototype.unflip = function () {
            //Makes the monster conscious
            this.alpha = 1;
            this.mGameWorld.game.tweens.remove(this.mFlipCueTween);
            this.mFlipped = false;
            this.mSpeed = Math.min(400, this.mSpeed + 30);
        };
        Monster.prototype.bounce = function () {
            this.body.velocity.y = -500;
            this.body.velocity.x = (2 * this.mSpeed * (this.mDirection - 1) + this.mSpeed) * 2;
            this.body.touching.down = false;
            if (this.mFlipped) {
                this.unflip();
                this.mGame.time.events.remove(this.mFlipTimer);
                this.mGame.time.events.remove(this.mFlipCueTimer);
            }
            else {
                this.flip();
            }
        };
        return Monster;
    }(Phaser.Sprite));
    MarioSis.Monster = Monster;
    var Peach = (function (_super) {
        __extends(Peach, _super);
        function Peach(world, x, y, d, gid) {
            var _this = _super.call(this, world, x, y, d, 'Peach', gid) || this;
            _this.mLife = 10;
            _this.mType = MonsterType.PEACH;
            _this.animations.add('WalkLeft', [0, 1, 2], 1, true);
            _this.animations.add('WalkRight', [3, 4, 5], 1, true);
            _this.animations.add('Flipped', [1], 1, true);
            _this.mSpeed = 220;
            _this.mFlipTimer = _this.mGame.time.events.add(20 * Phaser.Timer.SECOND, _this.teleport, _this);
            return _this;
        }
        Peach.prototype.update = function () {
            if (this.body.touching.down)
                this.body.velocity.y = 0;
            if (this.mFlipped) {
                this.animations.play('Flipped');
            }
            else if (this.mDirection == Direction.LEFT && this.mSpeed != 0) {
                this.body.velocity.x = -this.mSpeed;
                this.animations.play('WalkLeft');
            }
            else if (this.mDirection == Direction.RIGHT && this.mSpeed != 0) {
                this.body.velocity.x = this.mSpeed;
                this.animations.play('WalkRight');
            }
            this.wrap();
            this.isStandingOnGround = false;
        };
        Peach.prototype.wrap = function () {
            //Check if the body is escaping the left boundary of the World
            if (this.body.position.x + this.body.width - 5 < this.mGame.world.bounds.left) {
                this.position.x = this.mGame.world.bounds.right + this.getBounds().width / 2 - 15;
            }
            else if (this.body.position.x + 5 > this.mGame.world.bounds.right) {
                this.position.x = this.mGame.world.bounds.left - this.getBounds().width / 2 + 15;
            }
        };
        Peach.prototype.teleport = function () {
            var tween1 = this.mGameWorld.game.add.tween(this).to({ alpha: 0 }, 1500, Phaser.Easing.Linear.None);
            var tween2 = this.mGameWorld.game.add.tween(this).to({ x: 0, y: 0 }, 1, Phaser.Easing.Linear.None);
            var tween3 = this.mGameWorld.game.add.tween(this).to({ alpha: 1 }, 1500, Phaser.Easing.Linear.None);
            var a = this.mSpeed;
            this.mSpeed = 0;
            tween1.chain(tween2);
            tween2.chain(tween3);
            tween3.onComplete.add(function () { this.mSpeed = Math.min(600, a + 50); }, this);
            tween1.start();
            this.mFlipTimer = this.mGame.time.events.add(25 * Phaser.Timer.SECOND, this.teleport, this);
        };
        Peach.prototype.flip = function () {
            //Makes the monster unconscious
            this.mLife--;
            if (this.mLife <= 0) {
                this.mSpeed = 0;
                this.animations.play('Flipped');
                this.mGame.time.events.remove(this.mFlipTimer);
            }
        };
        return Peach;
    }(Monster));
    MarioSis.Peach = Peach;
    var Pumpkin = (function (_super) {
        __extends(Pumpkin, _super);
        function Pumpkin(world, x, y, d, gid) {
            var _this = _super.call(this, world, x, y, d, MarioSis.MonsterSpriteName.PUMPKIN, gid) || this;
            _this.mType = MonsterType.PUMPKIN;
            _this.animations.add('WalkLeft', [9, 7, 10, 7], 10, true);
            _this.animations.add('WalkRight', [8, 11, 8, 12], 10, true);
            _this.animations.add('Flipped', [6], 1, true);
            _this.animations.add('GlowRight', [1, 5, 2, 5], 10, true);
            _this.animations.add('GlowLeft', [0, 4, 3, 4], 10, true);
            _this.mGlowBody = new Phaser.Rectangle(46, 25, 65, 76);
            _this.body.setSize(65, 76, 0, 0);
            return _this;
        }
        return Pumpkin;
    }(Monster));
    MarioSis.Pumpkin = Pumpkin;
    var Ghost = (function (_super) {
        __extends(Ghost, _super);
        function Ghost(world, x, y, d, gid) {
            var _this = _super.call(this, world, x, y, d, MarioSis.MonsterSpriteName.GHOST, gid) || this;
            _this.mType = MonsterType.GHOST;
            _this.animations.add('WalkLeft', [0], 1, true);
            _this.animations.add('WalkRight', [1], 1, true);
            _this.animations.add('Flipped', [2], 1, true);
            _this.animations.add('GlowRight', [3], 1, true);
            _this.animations.add('GlowLeft', [4], 1, true);
            _this.mGlowBody = new Phaser.Rectangle(61, 26, 44, 69);
            _this.body.setSize(44, 69, 0, 0);
            _this.body.position.x = world.game.rnd.integer() % 1000;
            _this.position.x = _this.body.position.x;
            return _this;
        }
        return Ghost;
    }(Monster));
    MarioSis.Ghost = Ghost;
    var CandyCorn = (function (_super) {
        __extends(CandyCorn, _super);
        function CandyCorn(world, x, y, d, gid) {
            var _this = _super.call(this, world, x, y, d, MarioSis.MonsterSpriteName.BULLET, gid) || this;
            _this.mType = MonsterType.BULLET;
            _this.animations.add('WalkLeft', [1], 1, true);
            _this.animations.add('WalkRight', [0], 1, true);
            _this.animations.add('GlowRight', [2], 1, true);
            _this.animations.add('GlowLeft', [3], 1, true);
            _this.mGlowBody = new Phaser.Rectangle(38, 36, 128, 72);
            _this.body.setSize(128, 72, 0, 0);
            _this.body.gravity.y = 0;
            return _this;
        }
        CandyCorn.prototype.update = function () {
            if (this.mDirection == Direction.LEFT && this.mSpeed != 0) {
                this.body.velocity.x = -this.mSpeed;
                if (this.mKey)
                    this.animations.play('GlowLeft');
                else
                    this.animations.play('WalkLeft');
            }
            else if (this.mDirection == Direction.RIGHT && this.mSpeed != 0) {
                this.body.velocity.x = this.mSpeed;
                if (this.mKey)
                    this.animations.play('GlowRight');
                else
                    this.animations.play('WalkRight');
            }
            this.wrap();
        };
        CandyCorn.prototype.wrap = function () {
            //Check if the body is escaping the left boundary of the World
            if (this.body.position.x < this.mGame.world.bounds.left) {
                this.changeDirectionTo(Direction.RIGHT);
                this.body.position.x = this.mGame.world.bounds.left;
            }
            else if (this.body.position.x + this.body.width > this.mGame.world.bounds.right) {
                this.changeDirectionTo(Direction.LEFT);
                this.body.position.x = this.mGame.world.bounds.right - this.body.width;
            }
        };
        return CandyCorn;
    }(Monster));
    MarioSis.CandyCorn = CandyCorn;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var SpawnPoint = (function () {
        function SpawnPoint(x, y, direction) {
            this.x = x;
            this.y = y;
            this.direction = direction;
        }
        return SpawnPoint;
    }());
    MarioSis.SpawnPoint = SpawnPoint;
    var MonsterManager = (function () {
        function MonsterManager(world) {
            this.mMonsters = [];
            this.mCached = [];
            this.SpawnPoints = [];
            this.spawnedCnt = [];
            this.mWorld = world;
            this.total = 0;
            for (var i = 0; i < world.mLevelInfo.layers[1].objects.length; i++) {
                var data = world.mLevelInfo.layers[1].objects[i];
                this.SpawnPoints.push(new SpawnPoint(data.x, data.y, data.properties.Direction));
            }
            MarioSis.GameDataReader.Instance.Init(this.mWorld.game);
            this.startSpawn(this.mWorld.mLevel - 1);
        }
        MonsterManager.prototype.startSpawn = function (level) {
            var _this = this;
            this.spawnedCnt = [];
            var infos = MarioSis.GameDataReader.Instance.ReadLvlInfo(level);
            var _loop_1 = function () {
                var info = infos[i];
                info.mIdx = i;
                this_1.spawnedCnt.push(0);
                this_1.createMonster(info);
                MarioSis.TimerListener.Instance.Add(info.mCoolDownTime * 1000, function () {
                    _this.createMonster(info);
                });
            };
            var this_1 = this;
            for (var i = 0; i < infos.length; i++) {
                _loop_1();
            }
        };
        //Add More Types
        MonsterManager.prototype.createMonster = function (info) {
            if (this.spawnedCnt[info.mIdx] < info.mEnemyCnt) {
                var idx = this.spawnedCnt[info.mIdx] % 2;
                var m = this.enemyFactory(info.mEnemyType, this.SpawnPoints[idx], info.mIdx);
                this.mMonsters.push(m);
                this.spawnedCnt[info.mIdx]++;
            }
        };
        MonsterManager.prototype.createGhostMon = function (position) {
            //  let m = this.enemyFactory(MonsterType.GHOST, position);
            //this.mMonsters.push(m);
        };
        MonsterManager.prototype.enemyFactory = function (type, position, lvlGrpIdx) {
            var mon;
            switch (type) {
                case 0:
                    //should be pumpkin
                    mon = new MarioSis.Pumpkin(this.mWorld, position.x, position.y, position.direction, lvlGrpIdx);
                    break;
                case 1:
                    mon = new MarioSis.Ghost(this.mWorld, position.x, position.y, position.direction, lvlGrpIdx);
                    break;
                case 2:
                    mon = new MarioSis.CandyCorn(this.mWorld, position.x, position.y, position.direction, lvlGrpIdx);
                    break;
                case 3:
                    mon = new MarioSis.Peach(this.mWorld, position.x, position.y, position.direction, lvlGrpIdx);
                    break;
                default:
            }
            return mon;
        };
        MonsterManager.prototype.update = function () {
        };
        MonsterManager.prototype.removeMonster = function (key) {
            var index = -1;
            index = this.mMonsters.indexOf(key, 0);
            if (index > -1) {
                this.mMonsters.splice(index, 1);
            }
        };
        MonsterManager.prototype.updateMonNum = function (gid) {
            if (this.spawnedCnt != undefined && this.spawnedCnt.length >= gid) {
                this.spawnedCnt[gid]--;
            }
        };
        return MonsterManager;
    }());
    MarioSis.MonsterManager = MonsterManager;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var Platform = (function () {
        function Platform(world) {
            this.mWorld = world;
            this.mTiles = this.mWorld.game.add.group();
            this.mWeapons = this.mWorld.game.add.group();
            this.loadLevel();
        }
        Platform.prototype.loadLevel = function () {
            var mLevelData = this.mWorld.mLevelInfo;
            for (var i = 0; i < mLevelData.layers[0].data.length; i++) {
                var e = mLevelData.layers[0].data[i];
                if (e == 0)
                    continue;
                else {
                    var y = Math.floor(i / mLevelData.layers[0].width);
                    var x = i % mLevelData.layers[0].width;
                    var w = mLevelData.tilewidth;
                    var h = mLevelData.tileheight;
                    this.mTiles.add(new MarioSis.Tile(this, x * w, y * h, e - 1));
                }
            }
        };
        return Platform;
    }());
    MarioSis.Platform = Platform;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var Player = (function (_super) {
        __extends(Player, _super);
        function Player(gameWorld) {
            var _this = _super.call(this, gameWorld.game, gameWorld.game.world.bounds.width / 2, gameWorld.game.world.bounds.bottom - 100, 'Maria') || this;
            _this.mIsInvincible = true;
            _this.mWorld = gameWorld;
            _this.anchor.setTo(0.5, 0);
            _this.animations.add('Walking', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], 12, true);
            //Set up Physics
            _this.mWorld.game.physics.arcade.enable(_this);
            _this.body.gravity.y = 2000;
            _this.body.collideWorldBounds = true;
            _this.body.x = 10;
            _this.body.y = 1;
            _this.body.width = 40;
            _this.body.height = 102;
            _this.mInvincibleTween = _this.mWorld.game.add.tween(_this).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true, 0, 2);
            _this.mInvincibleTween.onComplete.add(function () { this.alpha = 1; this.mIsInvincible = false; }, _this);
            return _this;
        }
        Player.prototype.wrap = function () {
            if (this.body.position.x + this.body.width - 5 < this.mWorld.game.world.bounds.left) {
                this.position.x = this.mWorld.game.world.bounds.right + this.getBounds().width / 2 - 15;
            }
            else if (this.body.position.x + 5 > this.mWorld.game.world.bounds.right) {
                this.position.x = this.mWorld.game.world.bounds.left - this.getBounds().width / 2 + 15;
            }
        };
        Player.prototype.update = function () {
            this.updateInput();
            this.wrap();
        };
        Player.prototype.updateInput = function () {
            this.body.velocity.x = 0;
            if (this.mWorld.game.input.keyboard.isDown(Phaser.Keyboard.LEFT)) {
                this.animations.play('Walking');
                if (this.scale.x == -1) {
                    this.scale.x = 1;
                    this.body.position.x += this.body.width;
                }
                else {
                    this.body.velocity.x = -250;
                }
            }
            else if (this.mWorld.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT)) {
                this.animations.play('Walking');
                if (this.scale.x == 1) {
                    this.scale.x = -1;
                    this.body.position.x -= this.body.width;
                }
                else {
                    this.body.velocity.x = 250;
                }
            }
            else {
                this.animations.frame = 13;
            }
            if (this.mWorld.game.input.keyboard.isDown(Phaser.Keyboard.UP) && this.body.blocked.down) {
                this.body.velocity.y = -1000;
                MarioSis.AudioPlayer.Instance.playAudio(MarioSis.AudioType.JUMP);
            }
        };
        return Player;
    }(Phaser.Sprite));
    MarioSis.Player = Player;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var PlayerManager = (function () {
        function PlayerManager(gameWorld) {
            this.mGameWorld = gameWorld;
            this.mLife = 5;
            this.mFireKey = this.mGameWorld.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        }
        PlayerManager.prototype.createNewPlayer = function () {
            if (this.mLife == 0)
                return false;
            else {
                this.mLife--;
                this.mPlayer = new MarioSis.Player(this.mGameWorld);
                this.mWeapon = new MarioSis.WeaponManager(this.mPlayer);
                this.mGameWorld.game.add.existing(this.mPlayer);
                this.mGameWorld.game.camera.follow(this.mPlayer);
            }
        };
        PlayerManager.prototype.destroyPlayer = function () {
            if (this.mPlayer.mIsInvincible)
                return;
            this.mPlayer.destroy();
            this.mPlayer = undefined;
            if (this.mLife > 0) {
                MarioSis.AudioPlayer.Instance.playAudio(MarioSis.AudioType.SHOOT);
                this.mGameWorld.game.time.events.add(3 * Phaser.Timer.SECOND, this.createNewPlayer, this);
            }
            else {
                MarioSis.AudioPlayer.Instance.playAudio(MarioSis.AudioType.PLAYER_DEATH);
            }
        };
        PlayerManager.prototype.update = function () {
            if (this.mFireKey.justUp && this.mWeapon != null) {
                this.mWeapon.throwItem();
            }
            this.mWeapon.update();
        };
        return PlayerManager;
    }());
    MarioSis.PlayerManager = PlayerManager;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var Tile = (function (_super) {
        __extends(Tile, _super);
        function Tile(platform, x, y, type) {
            var _this = _super.call(this, platform.mWorld.game, x, y, 'Tiles', type) || this;
            _this.mBouncing = false;
            _this.mVelocity = -3;
            _this.mPlatform = platform;
            _this.mPlatform.mWorld.game.physics.arcade.enable(_this);
            _this.body.immovable = true;
            return _this;
        }
        Tile.prototype.bounce = function () {
            if (this.mBouncing == false) {
                this.mBouncing = true;
                this.mVelocity = -3;
            }
        };
        Tile.prototype.update = function () {
            if (this.mBouncing == true) {
                this.position.y += this.mVelocity;
                this.mVelocity += 0.5;
                if (this.mVelocity > 3)
                    this.mBouncing = false;
            }
        };
        return Tile;
    }(Phaser.Sprite));
    MarioSis.Tile = Tile;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Boot.prototype.preload = function () {
            this.load.image('preloadBar', 'assets/loader.png');
        };
        Boot.prototype.create = function () {
            //  Unless you specifically need to support multitouch I would recommend setting this to 1
            this.input.maxPointers = 1;
            //  Phaser will automatically pause if the browser tab the game is in loses focus. You can disable that here:
            this.stage.disableVisibilityChange = true;
            this.game.state.start('Preloader', true, false);
        };
        return Boot;
    }(Phaser.State));
    MarioSis.Boot = Boot;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var CreditList = (function (_super) {
        __extends(CreditList, _super);
        function CreditList() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        CreditList.prototype.create = function () {
            //this.text = this.game.add.text(this.game.world.centerX, this.game.height,
            //    "Credit \n\n Producer\n CORBIN WHITE \n\n Artist\n SARAH SOLLER \n\nTech Artist\n RIKKI ZHUANG \n\nEngineer\nEKSHIT NALWAYA\n\n Engineer\nCHEN MI"
            //    , { font: "42px Arial", fill: "#ffffff", align: "center" });
            this.text = this.add.sprite(0, 0, 'Credits');
            this.text.position.x = this.game.world.centerX - this.text.width / 2;
            this.text.position.y = this.game.world.height;
            //  var tween = this.add.tween(this.text).to({ y: 50}, 5000);
            //       tween.onComplete.add(this.startMainMenu, this);
            this.game.add.tween(this.text).to({ y: 200 }, 4000, Phaser.Easing.Linear.None, true);
        };
        CreditList.prototype.update = function () {
            _super.prototype.update.call(this);
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
                this.startMainMenu();
            }
        };
        CreditList.prototype.startMainMenu = function () {
            this.game.state.start('LevelMenu', true, false, 1);
        };
        return CreditList;
    }(Phaser.State));
    MarioSis.CreditList = CreditList;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var GameOver = (function (_super) {
        __extends(GameOver, _super);
        function GameOver() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        GameOver.prototype.create = function () {
            this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
            MarioSis.AudioPlayer.Instance.stopBGM();
            MarioSis.AudioPlayer.Instance.playAudio(MarioSis.AudioType.PLAYER_DEATH);
            this.background = this.add.sprite(0, 0, 'GameOver');
            this.background.width = 1920;
            this.background.height = 1080;
            this.background.alpha = 0;
            this.add.tween(this.background).to({ alpha: 1 }, 2000, Phaser.Easing.Bounce.InOut, true);
        };
        GameOver.prototype.fadeOut = function () {
            this.add.tween(this.background).to({ alpha: 0 }, 2000, Phaser.Easing.Linear.None, true).onComplete.add(this.startGame, this);
        };
        GameOver.prototype.update = function () {
            _super.prototype.update.call(this);
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
                this.fadeOut();
            }
        };
        GameOver.prototype.startGame = function () {
            this.game.state.start('LevelMenu', true, false, 1);
        };
        return GameOver;
    }(Phaser.State));
    MarioSis.GameOver = GameOver;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var GameWorld = (function (_super) {
        __extends(GameWorld, _super);
        function GameWorld() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        GameWorld.prototype.init = function (level) {
            this.mLevel = level;
        };
        GameWorld.prototype.create = function () {
            this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
            //Setup Background Music
            //this.mMusic = this.add.audio('BGM', 1, true);
            //this.mMusic.play();
            //Setup Background
            var sprite = this.game.add.sprite(0, 0, 'GameBackground');
            sprite.width = 1920;
            sprite.height = 1080;
            //Setup World Bounds
            this.game.world.setBounds(0, 0, 1920, 972);
            //Setup Physics
            this.physics.startSystem(Phaser.Physics.ARCADE);
            this.physics.startSystem(Phaser.Physics.P2JS);
            this.game.physics.arcade.checkCollision.left = false;
            this.game.physics.arcade.checkCollision.right = false;
            //Setup Level
            this.mLevelInfo = JSON.parse(this.game.cache.getText('Level' + this.mLevel.toString()));
            this.mPlatform = new MarioSis.Platform(this);
            //Setup Player
            this.mPlayerManager = new MarioSis.PlayerManager(this);
            this.mPlayerManager.createNewPlayer();
            this.mKeysCollected = 0;
            //Setup Monsters
            this.mMonsterManager = new MarioSis.MonsterManager(this);
            //Setup Keys
            this.mKeyManager = new MarioSis.KeyManager(this);
            this.mKeyManager.createNewKey();
            this.mHUD = new MarioSis.HUD(this);
        };
        GameWorld.prototype.update = function () {
            /*****************************
                    Collision Checks
            *****************************/
            //Platform Collisions: Player, Monster, Key, Self
            this.game.physics.arcade.collide(this.mPlayerManager.mPlayer, this.mPlatform.mTiles, this.collidePlayerAndTile, null, this);
            this.game.physics.arcade.collide(this.mMonsterManager.mMonsters, this.mPlatform.mTiles, this.collideMonsterAndTile, null, this);
            this.game.physics.arcade.collide(this.mKeyManager.mKeys, this.mPlatform.mTiles, this.collideKeyAndTile, null, this);
            this.game.physics.arcade.collide(this.mPlatform.mTiles, this.mPlatform.mTiles, this.collideTileAndTile, null, this);
            //Player VS Monster
            this.game.physics.arcade.collide(this.mPlayerManager.mPlayer, this.mMonsterManager.mMonsters, this.collidePlayerAndMonster, null, this);
            //Player VS Key
            this.game.physics.arcade.collide(this.mPlayerManager.mPlayer, this.mKeyManager.mKeys, this.collidePlayerAndKey, null, this);
            //Monster VS Key
            this.game.physics.arcade.overlap(this.mMonsterManager.mMonsters, this.mKeyManager.mKeys, this.collideMonsterAndKey, null, this);
            //Weapon VS Monster
            this.game.physics.arcade.collide(this.mMonsterManager.mMonsters, this.mPlayerManager.mWeapon.mWeapons, this.collideWeaponAndMonster, null, this);
            //Monster VS Monster
            this.game.physics.arcade.overlap(this.mMonsterManager.mMonsters, this.mMonsterManager.mMonsters, this.collideMonsterAndMonster, null, this);
            this.mMonsterManager.update();
            this.mPlayerManager.update();
            this.mHUD.update();
            if (this.mKeysCollected >= this.mLevelInfo.properties.KeyCount)
                this.game.state.start('LevelMenu', true, false, this.mLevel + 1);
            if (this.mPlayerManager.mLife <= 0) {
                this.endGame();
            }
        };
        GameWorld.prototype.endGame = function () {
            this.game.state.start('GameOver', true, false, 1);
        };
        GameWorld.prototype.collideMonsterAndKey = function (monster, key) {
            if (!monster.mKey && !monster.mFlipped) {
                monster.grabKey(key);
                this.mKeyManager.mKeys.remove(key);
            }
            else {
                var x = this.game.physics.arcade.getOverlapX(monster.body, key.body);
                var y = this.game.physics.arcade.getOverlapY(monster.body, key.body);
                x *= 0.5;
                if (monster.x < key.x) {
                    monster.body.x -= x;
                    key.body.x += x;
                }
                else {
                    key.body.x -= x;
                    monster.body.x += x;
                }
                if (monster.y < key.y) {
                    monster.body.y -= y;
                }
                else {
                    key.body.y -= y;
                }
                if (y > 0) {
                    if (monster.x < key.x) {
                        monster.body.x -= x;
                        key.body.x += x;
                    }
                    else {
                        key.body.x -= x;
                        monster.body.x += x;
                    }
                }
                if (monster.position.x < key.position.x) {
                    monster.changeDirectionTo(MarioSis.Direction.LEFT);
                }
                else {
                    monster.changeDirectionTo(MarioSis.Direction.RIGHT);
                }
                return false;
            }
            return false;
        };
        GameWorld.prototype.collideKeyAndTile = function (key, tile) {
            key.body.velocity.x = 0;
            key.body.velocity.y = 0;
        };
        GameWorld.prototype.collidePlayerAndKey = function (player, key) {
            this.mKeyManager.mKeys.remove(key, true);
            this.mKeyManager.mKeyCount--;
            this.mKeysCollected++;
            MarioSis.AudioPlayer.Instance.playAudio(MarioSis.AudioType.PICK_UP);
            return true;
        };
        GameWorld.prototype.collideTileAndTile = function (tile1, tile2) {
            if (tile1.mBouncing && tile1.mVelocity == 0)
                tile2.bounce();
            else if (tile2.mBouncing && tile2.mVelocity == 0)
                tile1.bounce();
            return false;
        };
        GameWorld.prototype.collidePlayerAndTile = function (player, tile) {
            if (player.body.touching.down == true && tile.body.touching.up == true) {
                player.body.blocked.down = true;
                player.body.velocity.y = 0;
            }
            else if (player.body.touching.up == true && tile.body.touching.down == true) {
                player.body.velocity.y = 0;
                tile.bounce();
                MarioSis.AudioPlayer.Instance.playAudio(MarioSis.AudioType.BlockHit);
            }
            return true;
        };
        GameWorld.prototype.collidePlayerAndMonster = function (player, monster) {
            if (monster.mFlipped == true) {
                monster.remove();
                MarioSis.AudioPlayer.Instance.playAudio(MarioSis.AudioType.MON_DEATH);
            }
            else
                this.mPlayerManager.destroyPlayer();
            return true;
        };
        GameWorld.prototype.collideWeaponAndMonster = function (monster, weapon) {
            monster.flip();
            monster.remove();
            weapon.destroy();
            this.mPlayerManager.mWeapon.removeWeapon(weapon);
            this.mMonsterManager.removeMonster(monster);
            MarioSis.AudioPlayer.Instance.playAudio(MarioSis.AudioType.MON_DEATH);
            return true;
        };
        GameWorld.prototype.collideMonsterAndMonster = function (monster1, monster2) {
            var x = this.game.physics.arcade.getOverlapX(monster1.body, monster2.body);
            var y = this.game.physics.arcade.getOverlapY(monster1.body, monster2.body);
            x *= 0.5;
            if (monster1.x < monster2.x) {
                monster1.body.x -= x;
                monster2.body.x += x;
            }
            else {
                monster2.body.x -= x;
                monster1.body.x += x;
            }
            if (monster1.y < monster2.y) {
                monster1.body.y -= y;
            }
            else {
                monster2.body.y -= y;
            }
            if (y > 0) {
                if (monster1.x < monster2.x) {
                    monster1.body.x -= x;
                    monster2.body.x += x;
                }
                else {
                    monster2.body.x -= x;
                    monster1.body.x += x;
                }
            }
            if (monster1.position.x < monster2.position.x) {
                monster1.changeDirectionTo(MarioSis.Direction.LEFT);
                monster2.changeDirectionTo(MarioSis.Direction.RIGHT);
            }
            else {
                monster1.changeDirectionTo(MarioSis.Direction.RIGHT);
                monster2.changeDirectionTo(MarioSis.Direction.LEFT);
            }
            return false;
        };
        GameWorld.prototype.collideMonsterAndTile = function (monster, tile) {
            if (tile.mBouncing) {
                monster.bounce();
                return;
            }
            monster.body.velocity.x = 0;
            monster.body.velocity.y = 0;
            //If the monster is standing on top of something and collides with a tile, it means it collided with a wall.
            //So, reverse the direction.
            if (monster.body.touching.right == true && tile.body.touching.left == true) {
                monster.changeDirectionTo(MarioSis.Direction.LEFT);
            }
            else if (monster.body.touching.left == true && tile.body.touching.right == true) {
                monster.changeDirectionTo(MarioSis.Direction.RIGHT);
            }
            return true;
        };
        return GameWorld;
    }(Phaser.State));
    MarioSis.GameWorld = GameWorld;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var LevelMenu = (function (_super) {
        __extends(LevelMenu, _super);
        function LevelMenu() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        LevelMenu.prototype.init = function (level) {
            this.mLevel = level;
            var mLevelInfo = JSON.parse(this.game.cache.getText('Level' + this.mLevel.toString()));
            if (mLevelInfo == undefined) {
                this.game.state.start('CreditList', true, false, 1);
            }
        };
        LevelMenu.prototype.create = function () {
            if (this.mLevel == 3)
                this.mVideo = this.game.add.video('PeachVideo');
            else if (this.mLevel == 2)
                this.mVideo = this.game.add.video('TricksterVideo');
            else if (this.mLevel == 1)
                this.mVideo = this.game.add.video('PumpkinVideo');
            else {
                this.onVideoComplete();
                return;
            }
            MarioSis.AudioPlayer.Instance.stopBGM();
            this.mSprite = this.mVideo.addToWorld();
            this.mVideo.play(true);
            this.mVideo.loop = false;
            if (this.mLevel == 2)
                this.mVideo.onComplete.add(this.playAnotherVideo, this);
            else
                this.mVideo.onComplete.add(this.onVideoComplete, this);
        };
        LevelMenu.prototype.playAnotherVideo = function () {
            MarioSis.AudioPlayer.Instance.stopBGM();
            this.mVideo.onComplete.removeAll();
            this.mVideo.changeSource('./Assets/Video/Corny.mp4');
            this.mVideo.play(true);
            this.mVideo.loop = false;
            this.mVideo.onComplete.add(this.onVideoComplete, this);
        };
        LevelMenu.prototype.onVideoComplete = function () {
            this.mSprite.destroy();
            var mLevelInfo = JSON.parse(this.game.cache.getText('Level' + this.mLevel.toString()));
            var style = { font: "bold 32px Gothic", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };
            this.mLevelText = this.game.add.text(50, 100, "Level " + this.mLevel.toString(), style);
            this.mObjectiveText = this.game.add.text(50, 400, "Objective: " + mLevelInfo.properties.Objective, style);
            //this.mMonster = this.game.add.text(50, 700, "Monsters: ", style);
            this.mLevelText.addColor('#ffffff', 0);
            this.mObjectiveText.addColor('#ffffff', 0);
            //this.mMonster.addColor('#ffffff', 0);
            this.mLevelText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
            this.mObjectiveText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
            //this.mMonster.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
            this.mLevelText.setTextBounds(0, 0, 1920, 100);
            this.mObjectiveText.setTextBounds(0, 0, 1920, 100);
            //this.mMonster.setTextBounds(0, 0, 1920, 100);
            MarioSis.AudioPlayer.Instance.playBGM(this.mLevel == 3);
        };
        LevelMenu.prototype.update = function () {
            _super.prototype.update.call(this);
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && this.mVideo.playing) {
                this.mVideo.onComplete.removeAll();
                this.mVideo.stop();
                this.mVideo.destroy();
                this.mSprite.destroy();
                this.onVideoComplete();
            }
            else if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
                this.fadeOut();
            }
        };
        LevelMenu.prototype.fadeOut = function () {
            this.add.tween(this.mLevelText).to({ alpha: 0 }, 1500, Phaser.Easing.Linear.None, true);
            this.add.tween(this.mObjectiveText).to({ alpha: 0 }, 1500, Phaser.Easing.Linear.None, true).onComplete.add(this.startGame, this);
            //this.add.tween(this.mMonster).to({ alpha: 0 }, 1500, Phaser.Easing.Linear.None, true).onComplete.add(this.startGame, this);
        };
        LevelMenu.prototype.startGame = function () {
            this.game.state.start('GameWorld', true, false, this.mLevel);
        };
        return LevelMenu;
    }(Phaser.State));
    MarioSis.LevelMenu = LevelMenu;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var MainMenu = (function (_super) {
        __extends(MainMenu, _super);
        function MainMenu() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        MainMenu.prototype.create = function () {
            this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.background = this.add.sprite(0, 0, 'TitleBackground');
            this.background.width = 1920;
            this.background.height = 1080;
            this.background.alpha = 0;
            this.logo = this.add.sprite(this.world.centerX, -300, 'TitleName');
            this.logo.anchor.setTo(0.5, 0.5);
            this.add.tween(this.background).to({ alpha: 1 }, 2000, Phaser.Easing.Bounce.InOut, true);
            this.add.tween(this.logo).to({ y: 220 }, 2000, Phaser.Easing.Elastic.Out, true, 2000);
        };
        MainMenu.prototype.fadeOut = function () {
            this.add.tween(this.background).to({ alpha: 0 }, 2000, Phaser.Easing.Linear.None, true);
            var tween = this.add.tween(this.logo).to({ y: 800 }, 2000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.startGame, this);
        };
        MainMenu.prototype.update = function () {
            _super.prototype.update.call(this);
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
                this.fadeOut();
            }
        };
        MainMenu.prototype.startGame = function () {
            this.game.state.start('LevelMenu', true, false, 1);
        };
        return MainMenu;
    }(Phaser.State));
    MarioSis.MainMenu = MainMenu;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Preloader.prototype.preload = function () {
            //  Set-up our preloader sprite
            this.preloadBar = this.add.sprite(200, 250, 'preloadBar');
            this.load.setPreloadSprite(this.preloadBar);
            //Load Title-Screen
            this.load.image('TitleBackground', './Assets/TitleBackground.png');
            this.load.image('TitleName', './Assets/TitleName.png');
            this.load.image('GameOver', './Assets/GameOver.png');
            this.load.image('Credits', './Assets/Credit.png');
            //Load Video
            this.load.video('Intro', './Assets/Video/Intro.mp4');
            //Load Levels
            this.load.text('Level1', './Assets/Level1.json');
            this.load.text('Level2', './Assets/Level2.json');
            this.load.text('Level3', './Assets/Level3.json');
            this.load.json("GameSetting", "./Assets/GameData/GameData.json");
            //Load Intermissions
            this.load.video('PeachVideo', './Assets/Video/Peach.mp4');
            this.load.video('CornyVideo', './Assets/Video/Corny.mp4');
            this.load.video('TricksterVideo', './Assets/Video/Trickster.mp4');
            this.load.video('PumpkinVideo', './Assets/Video/Pumpkin.mp4');
            //Load Character And Animations
            this.load.atlasJSONArray('Maria', './Assets/Maria.png', 'Assets/Maria.json');
            this.load.atlasJSONArray('Ghost', './Assets/Ghost.png', 'Assets/Ghost.json');
            this.load.atlasJSONArray('Pumpkin', './Assets/Pumpkin.png', 'Assets/Pumpkin.json');
            this.load.atlasJSONArray('CandyCorn', './Assets/CandyCorn.png', 'Assets/CandyCorn.json');
            this.load.atlasJSONArray('Peach', './Assets/Peach.png', 'Assets/Peach.json');
            //Load Weapon and Keys
            this.load.image('Heart', './Assets/Weapon/Heart.png');
            this.load.image("Candy", "./Assets/Candy.png");
            //Load Game Background, Tiles and HUD						
            this.load.image('GameBackground', './Assets/Background.png');
            this.load.spritesheet("Tiles", "./Assets/Tiles.png", 64, 36, 10);
            this.load.image('Ground', './Assets/Ground1.png');
            this.load.image('HUDLife', './Assets/HUDLife.png');
            this.load.image('HUDCandy', './Assets/HUDCandy.png');
            this.load.image('HUDWeapon', './Assets/HUDWeapon.png');
            this.load.image('HUDPeach', './Assets/HUDPeach.png');
            //Load Audio Files
            this.load.audio('PickKey', './Assets/Audio/Key Pickup.wav');
            this.load.audio('Jump', './Assets/Audio/Mario Jump.wav');
            this.load.audio('PlayerDeath', './Assets/Audio/evil_laugh.wav');
            this.load.audio('ShootSfx', './Assets/Audio/Player Death.mp3');
            this.load.audio('MonDeath', './Assets/Audio/Monster Death.mp3');
            this.load.audio('BlockHit', './Assets/Audio/Block Hit.wav');
            this.load.audio('BGM', './Assets/Audio/BGM.mp3');
            this.load.audio('BossBGM', './Assets/Audio/BossBGM.mp3');
        };
        Preloader.prototype.create = function () {
            this.game.scale.startFullScreen();
            var tween = this.add.tween(this.preloadBar).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.startMainMenu, this);
            MarioSis.TimerListener.Instance.InitTimer(this.game);
            MarioSis.AudioPlayer.Instance.Init(this.game);
        };
        Preloader.prototype.startMainMenu = function () {
            this.game.state.start('MainMenu', true, false);
        };
        return Preloader;
    }(Phaser.State));
    MarioSis.Preloader = Preloader;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var Tutorial = (function (_super) {
        __extends(Tutorial, _super);
        function Tutorial() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Tutorial.prototype.init = function (level) {
        };
        Tutorial.prototype.create = function () {
            var video = this.game.add.video('Intro');
            //  See the docs for the full parameters
            //  But it goes x, y, anchor x, anchor y, scale x, scale y
            var sprite = video.addToWorld(this.game.world.centerX, this.game.world.centerY, 0.5, 0.5, 2, 2);
            //  true = loop
            video.play(true);
        };
        Tutorial.prototype.update = function () {
        };
        Tutorial.prototype.startGame = function () {
            this.game.state.start('GameWorld', true, false, this.mLevel);
        };
        return Tutorial;
    }(Phaser.State));
    MarioSis.Tutorial = Tutorial;
})(MarioSis || (MarioSis = {}));
var MarioSis;
(function (MarioSis) {
    var VedioPlay = (function (_super) {
        __extends(VedioPlay, _super);
        function VedioPlay() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        VedioPlay.prototype.create = function () {
            this.video = this.game.add.video('Intro');
            this.video.play(true);
            //  x, y, anchor x, anchor y, scale x, scale y
            this.video.addToWorld(this.game.world.centerX - this.video.width / 2, this.game.world.centerY - this.video.height / 2);
        };
        VedioPlay.prototype.update = function () {
            _super.prototype.update.call(this);
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
                this.startGame();
            }
        };
        VedioPlay.prototype.startGame = function () {
            this.video.stop();
            this.game.state.start('GameWorld', true, false, 1);
        };
        return VedioPlay;
    }(Phaser.State));
    MarioSis.VedioPlay = VedioPlay;
})(MarioSis || (MarioSis = {}));
//# sourceMappingURL=Game.js.map