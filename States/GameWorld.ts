﻿module MarioSis
{
	export class GameWorld extends Phaser.State
	{
		mLevel: number;
		mLevelInfo: any;

		mBackGround: Phaser.Sprite;
		mMusic: Phaser.Sound;

		mPlayerManager: PlayerManager;
        mPlatform: Platform;
		mMonsterManager: MonsterManager;
		mKeyManager: KeyManager;

		mHUD: HUD;

		mKeysCollected: number;

		init(level: number)
		{
			this.mLevel = level;
		}

		create()
		{
			this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

			//Setup Background Music
			//this.mMusic = this.add.audio('BGM', 1, true);
			//this.mMusic.play();

			//Setup Background
		    var sprite = this.game.add.sprite(0, 0, 'GameBackground');
			sprite.width = 1920;
			sprite.height = 1080;

			//Setup World Bounds
			this.game.world.setBounds(0, 0, 1920, 972);
			
			//Setup Physics
			this.physics.startSystem(Phaser.Physics.ARCADE);
			this.physics.startSystem(Phaser.Physics.P2JS);
			this.game.physics.arcade.checkCollision.left = false;
			this.game.physics.arcade.checkCollision.right = false;
					

			//Setup Level
			this.mLevelInfo = JSON.parse(this.game.cache.getText('Level' + this.mLevel.toString()));
			this.mPlatform = new Platform(this);
            
			//Setup Player
			this.mPlayerManager = new PlayerManager(this);
			this.mPlayerManager.createNewPlayer();
			this.mKeysCollected = 0;

			//Setup Monsters
			this.mMonsterManager = new MonsterManager(this);
			
			//Setup Keys
			this.mKeyManager = new KeyManager(this);
			this.mKeyManager.createNewKey();

            this.mHUD = new HUD(this);
		}

		update()
		{
			/*****************************
					Collision Checks
			*****************************/

			//Platform Collisions: Player, Monster, Key, Self
			this.game.physics.arcade.collide(this.mPlayerManager.mPlayer,	this.mPlatform.mTiles, this.collidePlayerAndTile,	null, this);
			this.game.physics.arcade.collide(this.mMonsterManager.mMonsters,this.mPlatform.mTiles, this.collideMonsterAndTile,	null, this);
			this.game.physics.arcade.collide(this.mKeyManager.mKeys, this.mPlatform.mTiles, this.collideKeyAndTile,				null, this);
			this.game.physics.arcade.collide(this.mPlatform.mTiles,			this.mPlatform.mTiles, this.collideTileAndTile,		null, this);

			//Player VS Monster
			this.game.physics.arcade.collide(this.mPlayerManager.mPlayer, this.mMonsterManager.mMonsters, this.collidePlayerAndMonster, null, this);

			//Player VS Key
			this.game.physics.arcade.collide(this.mPlayerManager.mPlayer, this.mKeyManager.mKeys, this.collidePlayerAndKey, null, this);

			//Monster VS Key
			this.game.physics.arcade.overlap(this.mMonsterManager.mMonsters, this.mKeyManager.mKeys, this.collideMonsterAndKey, null, this);

		    //Weapon VS Monster
            this.game.physics.arcade.collide(this.mMonsterManager.mMonsters, this.mPlayerManager.mWeapon.mWeapons,this.collideWeaponAndMonster, null, this);

			//Monster VS Monster
			this.game.physics.arcade.overlap(this.mMonsterManager.mMonsters, this.mMonsterManager.mMonsters, this.collideMonsterAndMonster, null, this);	
			
            this.mMonsterManager.update();
			this.mPlayerManager.update();
			this.mHUD.update();

			if (this.mKeysCollected >= this.mLevelInfo.properties.KeyCount)
                this.game.state.start('LevelMenu', true, false, this.mLevel + 1);

            if (this.mPlayerManager.mLife <= 0) {
                this.endGame();
            }
        }

        endGame() {
            this.game.state.start('GameOver', true, false, 1);
        }

		collideMonsterAndKey(monster: Monster, key: Key)
		{
			if (!monster.mKey && !monster.mFlipped)
			{
				monster.grabKey(key);				
				this.mKeyManager.mKeys.remove(key);
			}
			else
			{
				var x = this.game.physics.arcade.getOverlapX(monster.body, key.body);
				var y = this.game.physics.arcade.getOverlapY(monster.body, key.body);

				x *= 0.5;
				if (monster.x < key.x)
				{
					monster.body.x -= x;
					key.body.x += x;
				}
				else
				{
					key.body.x -= x;
					monster.body.x += x;
				}

				if (monster.y < key.y)
				{
					monster.body.y -= y;
				}
				else
				{
					key.body.y -= y;
				}

				if (y > 0)
				{
					if (monster.x < key.x)
					{
						monster.body.x -= x;
						key.body.x += x;
					}
					else
					{
						key.body.x -= x;
						monster.body.x += x;
					}
				}

				if (monster.position.x < key.position.x)
				{
					monster.changeDirectionTo(Direction.LEFT);					
				}
				else
				{
					monster.changeDirectionTo(Direction.RIGHT);					
				}
				return false;
			}			
			return false;
		}

		collideKeyAndTile(key: Key, tile: Tile)
		{
			key.body.velocity.x = 0;
			key.body.velocity.y = 0;
		}

		collidePlayerAndKey(player: Player, key: Key)
		{
			this.mKeyManager.mKeys.remove(key, true);
			this.mKeyManager.mKeyCount--;
            this.mKeysCollected++;
          AudioPlayer.Instance.playAudio(AudioType.PICK_UP);
			return true;
		}		

		collideTileAndTile(tile1: Tile, tile2: Tile)
		{
			if (tile1.mBouncing && tile1.mVelocity == 0)
				tile2.bounce();
			else if (tile2.mBouncing && tile2.mVelocity == 0)
				tile1.bounce();
			return false;
		}
		
		collidePlayerAndTile(player: Player, tile: Tile)
		{
			if (player.body.touching.down == true && tile.body.touching.up == true)
			{
				player.body.blocked.down = true;
				player.body.velocity.y = 0;
			}
			else if (player.body.touching.up == true && tile.body.touching.down == true)
			{
				player.body.velocity.y = 0;
                tile.bounce();
			    AudioPlayer.Instance.playAudio(AudioType.BlockHit);
            }	
           
			return true;
		}

		collidePlayerAndMonster(player: Player, monster: Monster)
		{
			if (monster.mFlipped == true) {
			     monster.remove();
			    AudioPlayer.Instance.playAudio(AudioType.MON_DEATH);
			}
			else
				this.mPlayerManager.destroyPlayer();
			return true;
        }
		
		collideWeaponAndMonster(monster: Monster, weapon: Phaser.Sprite) 
		{
			monster.flip();
			monster.remove();
            weapon.destroy();
            this.mPlayerManager.mWeapon.removeWeapon(weapon);
            this.mMonsterManager.removeMonster(monster);
            AudioPlayer.Instance.playAudio(AudioType.MON_DEATH);
	        return true;
	    }

		collideMonsterAndMonster(monster1: Monster, monster2: Monster)
		{
			var x = this.game.physics.arcade.getOverlapX(monster1.body, monster2.body);
			var y = this.game.physics.arcade.getOverlapY(monster1.body, monster2.body);

			x *= 0.5;
			if (monster1.x < monster2.x)
			{
				monster1.body.x -= x;
				monster2.body.x += x;
			}
			else
			{
				monster2.body.x -= x;
				monster1.body.x += x;
			}
			
			if (monster1.y < monster2.y)
			{
				monster1.body.y -= y;
			}
			else
			{
				monster2.body.y -= y;
			}

			if (y > 0)
			{
				if (monster1.x < monster2.x)
				{
					monster1.body.x -= x;
					monster2.body.x += x;
				}
				else
				{
					monster2.body.x -= x;
					monster1.body.x += x;
				}
			}

			if (monster1.position.x < monster2.position.x)
			{
				monster1.changeDirectionTo(Direction.LEFT);
				monster2.changeDirectionTo(Direction.RIGHT);
			}
			else
			{
				monster1.changeDirectionTo(Direction.RIGHT);
				monster2.changeDirectionTo(Direction.LEFT);
			}	
			return false;
		}

		collideMonsterAndTile(monster: Monster, tile: Tile)
		{			
			if (tile.mBouncing)
			{
				monster.bounce();					
				return;
			}
			monster.body.velocity.x = 0;
			monster.body.velocity.y = 0;

			//If the monster is standing on top of something and collides with a tile, it means it collided with a wall.
			//So, reverse the direction.
			if (monster.body.touching.right == true && tile.body.touching.left == true )
			{
				monster.changeDirectionTo(Direction.LEFT);
			}
			else if (monster.body.touching.left == true && tile.body.touching.right == true)
			{
				monster.changeDirectionTo(Direction.RIGHT);
			}
			return true;
		}
	}
}