﻿module MarioSis {
    export class VedioPlay extends Phaser.State {
        background: Phaser.Sprite;
        logo: Phaser.Sprite;
        music: Phaser.Sound;
        video:Phaser.Video;
        create() {
            this.video = this.game.add.video('Intro');

            this.video.play(true);
            //  x, y, anchor x, anchor y, scale x, scale y
            this.video.addToWorld(this.game.world.centerX - this.video.width / 2, this.game.world.centerY - this.video.height / 2);
        }

        update(): void {
            super.update();
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
                this.startGame();
            }
        }


        startGame() {
            this.video.stop();
                this.game.state.start('GameWorld', true, false, 1);
        }
    }
}