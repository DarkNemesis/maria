﻿module MarioSis {
    export class CreditList extends Phaser.State {
        background: Phaser.Sprite;
        logo: Phaser.Sprite;
        music: Phaser.Sound;
        text:any;
        create() {
            //this.text = this.game.add.text(this.game.world.centerX, this.game.height,
            //    "Credit \n\n Producer\n CORBIN WHITE \n\n Artist\n SARAH SOLLER \n\nTech Artist\n RIKKI ZHUANG \n\nEngineer\nEKSHIT NALWAYA\n\n Engineer\nCHEN MI"
            //    , { font: "42px Arial", fill: "#ffffff", align: "center" });
            this.text = this.add.sprite(0, 0, 'Credits');
            this.text.position.x = this.game.world.centerX - this.text.width / 2;
            this.text.position.y = this.game.world.height; 
          //  var tween = this.add.tween(this.text).to({ y: 50}, 5000);
     //       tween.onComplete.add(this.startMainMenu, this);
            this.game.add.tween(this.text).to({ y: 200 }, 4000, Phaser.Easing.Linear.None, true);
        
        }

        update(): void {
            super.update();
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
                this.startMainMenu();
            }
        }


        startMainMenu() {

            this.game.state.start('LevelMenu', true, false, 1);

        }
    }
}