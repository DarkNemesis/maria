﻿module MarioSis
{
	export class LevelMenu extends Phaser.State
	{
		mLevel: number;
		mLevelText: Phaser.Text;
		mObjectiveText: Phaser.Text;
		//mMonster: Phaser.Text;

		mVideo: Phaser.Video;
		mSprite: Phaser.Image;
		init(level: number)
		{			
			this.mLevel = level;
			
			var mLevelInfo = JSON.parse(this.game.cache.getText('Level' + this.mLevel.toString()));

			if (mLevelInfo == undefined)
			{
				this.game.state.start('CreditList', true, false, 1);
			}
		}

		create()
		{
			if (this.mLevel == 3)
				this.mVideo = this.game.add.video('PeachVideo');
			else if (this.mLevel == 2)
				this.mVideo = this.game.add.video('TricksterVideo');
			else if (this.mLevel == 1)
				this.mVideo = this.game.add.video('PumpkinVideo');
			else
			{
				this.onVideoComplete();
				return;
			}
            AudioPlayer.Instance.stopBGM();
			this.mSprite = this.mVideo.addToWorld();
			this.mVideo.play(true);
			this.mVideo.loop = false;

			if (this.mLevel == 2)
				this.mVideo.onComplete.add(this.playAnotherVideo, this);
			else
				this.mVideo.onComplete.add(this.onVideoComplete, this);	
		}

		playAnotherVideo()
        {
            AudioPlayer.Instance.stopBGM();
			this.mVideo.onComplete.removeAll();

			this.mVideo.changeSource('./Assets/Video/Corny.mp4');
			this.mVideo.play(true);			
			this.mVideo.loop = false;
			
			this.mVideo.onComplete.add(this.onVideoComplete, this);	
		}

		onVideoComplete()
		{
			this.mSprite.destroy();
			
			var mLevelInfo = JSON.parse(this.game.cache.getText('Level' + this.mLevel.toString()));
			var style = { font: "bold 32px Gothic", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };

			this.mLevelText = this.game.add.text(50, 100, "Level " + this.mLevel.toString(), style);
			this.mObjectiveText = this.game.add.text(50, 400, "Objective: " + mLevelInfo.properties.Objective, style);
			//this.mMonster = this.game.add.text(50, 700, "Monsters: ", style);

			this.mLevelText.addColor('#ffffff', 0);
			this.mObjectiveText.addColor('#ffffff', 0);
			//this.mMonster.addColor('#ffffff', 0);
			
			this.mLevelText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
			this.mObjectiveText.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);
			//this.mMonster.setShadow(3, 3, 'rgba(0,0,0,0.5)', 2);

			this.mLevelText.setTextBounds(0, 0, 1920, 100);
			this.mObjectiveText.setTextBounds(0, 0, 1920, 100);
			//this.mMonster.setTextBounds(0, 0, 1920, 100);
            AudioPlayer.Instance.playBGM(this.mLevel == 3);
		}

		update(): void
		{
			super.update();
			if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR) && this.mVideo.playing)
			{
				this.mVideo.onComplete.removeAll();
				this.mVideo.stop();
				this.mVideo.destroy();
				this.mSprite.destroy();
				this.onVideoComplete();
			}
			else if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR))
			{				
				this.fadeOut();
			}

		}

		fadeOut()
		{
			this.add.tween(this.mLevelText).to({ alpha: 0 }, 1500, Phaser.Easing.Linear.None, true);
			this.add.tween(this.mObjectiveText).to({ alpha: 0 }, 1500, Phaser.Easing.Linear.None, true).onComplete.add(this.startGame, this);
			//this.add.tween(this.mMonster).to({ alpha: 0 }, 1500, Phaser.Easing.Linear.None, true).onComplete.add(this.startGame, this);
		}

		startGame()
		{
			this.game.state.start('GameWorld', true, false, this.mLevel);
		}
	}
}