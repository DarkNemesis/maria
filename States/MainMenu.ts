﻿module MarioSis
{
	export class MainMenu extends Phaser.State
	{
		background: Phaser.Sprite;
		logo: Phaser.Sprite;
		music: Phaser.Sound;
		create()
		{
			this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;

			this.background = this.add.sprite(0, 0, 'TitleBackground');
			this.background.width = 1920;
			this.background.height = 1080;
			this.background.alpha = 0;

			this.logo = this.add.sprite(this.world.centerX, -300, 'TitleName');
			this.logo.anchor.setTo(0.5, 0.5);

			 

			this.add.tween(this.background).to({ alpha: 1 }, 2000, Phaser.Easing.Bounce.InOut, true);
			this.add.tween(this.logo).to({ y: 220 }, 2000, Phaser.Easing.Elastic.Out, true, 2000);

		
		}

		fadeOut()
		{
			this.add.tween(this.background).to({ alpha: 0 }, 2000, Phaser.Easing.Linear.None, true);
			var tween = this.add.tween(this.logo).to({ y: 800 }, 2000, Phaser.Easing.Linear.None, true);

			tween.onComplete.add(this.startGame, this);
        }

	    update(): void {
            super.update();
            if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
                this.fadeOut();
            }

	    }

	    startGame() 
		{
            this.game.state.start('LevelMenu', true, false, 1);
		}
	}
}