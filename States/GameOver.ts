﻿module MarioSis
{
	export class GameOver extends Phaser.State
	{
		background: Phaser.Sprite;		
		music: Phaser.Sound;
		create()
		{
			this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
            AudioPlayer.Instance.stopBGM();
            AudioPlayer.Instance.playAudio(AudioType.PLAYER_DEATH);
			this.background = this.add.sprite(0, 0, 'GameOver');
			this.background.width = 1920;
			this.background.height = 1080;
			this.background.alpha = 0;
			
			this.add.tween(this.background).to({ alpha: 1 }, 2000, Phaser.Easing.Bounce.InOut, true);			
		 
		}

		fadeOut()
		{
			this.add.tween(this.background).to({ alpha: 0 }, 2000, Phaser.Easing.Linear.None, true).onComplete.add(this.startGame, this);
		}

		update(): void
		{
			super.update();
			if (this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR))
			{
				this.fadeOut();
			}

		}

		startGame() 
		{
            this.game.state.start('LevelMenu', true, false, 1);
		}
	}
}