﻿module MarioSis {

    export class Preloader extends Phaser.State {

        preloadBar: Phaser.Sprite;

        preload() {

            //  Set-up our preloader sprite
            this.preloadBar = this.add.sprite(200, 250, 'preloadBar');
            this.load.setPreloadSprite(this.preloadBar);

            //Load Title-Screen
			this.load.image('TitleBackground', './Assets/TitleBackground.png');
			this.load.image('TitleName', './Assets/TitleName.png');
			this.load.image('GameOver', './Assets/GameOver.png');
            this.load.image('Credits', './Assets/Credit.png');

              //Load Video
            this.load.video('Intro', './Assets/Video/Intro.mp4');

			//Load Levels
			this.load.text('Level1', './Assets/Level1.json');	
			this.load.text('Level2', './Assets/Level2.json');	
			this.load.text('Level3', './Assets/Level3.json');	
			this.load.json("GameSetting", "./Assets/GameData/GameData.json");

			//Load Intermissions
			this.load.video('PeachVideo', './Assets/Video/Peach.mp4');
			this.load.video('CornyVideo', './Assets/Video/Corny.mp4');
			this.load.video('TricksterVideo', './Assets/Video/Trickster.mp4');
			this.load.video('PumpkinVideo', './Assets/Video/Pumpkin.mp4');


			//Load Character And Animations
			this.load.atlasJSONArray('Maria', './Assets/Maria.png', 'Assets/Maria.json');
			this.load.atlasJSONArray('Ghost', './Assets/Ghost.png', 'Assets/Ghost.json');
			this.load.atlasJSONArray('Pumpkin', './Assets/Pumpkin.png', 'Assets/Pumpkin.json');
			this.load.atlasJSONArray('CandyCorn', './Assets/CandyCorn.png', 'Assets/CandyCorn.json');
			this.load.atlasJSONArray('Peach', './Assets/Peach.png', 'Assets/Peach.json');

			//Load Weapon and Keys
            this.load.image('Heart', './Assets/Weapon/Heart.png');
			this.load.image("Candy", "./Assets/Candy.png");

			//Load Game Background, Tiles and HUD						
			this.load.image('GameBackground', './Assets/Background.png');
            this.load.spritesheet("Tiles", "./Assets/Tiles.png", 64, 36, 10);	
			this.load.image('Ground', './Assets/Ground1.png');		
			this.load.image('HUDLife', './Assets/HUDLife.png');	
			this.load.image('HUDCandy', './Assets/HUDCandy.png');	
			this.load.image('HUDWeapon', './Assets/HUDWeapon.png');	
			this.load.image('HUDPeach', './Assets/HUDPeach.png');	

            //Load Audio Files
            this.load.audio('PickKey', './Assets/Audio/Key Pickup.wav');
            this.load.audio('Jump', './Assets/Audio/Mario Jump.wav');
            this.load.audio('PlayerDeath', './Assets/Audio/evil_laugh.wav');
            this.load.audio('ShootSfx', './Assets/Audio/Player Death.mp3');
            this.load.audio('MonDeath', './Assets/Audio/Monster Death.mp3');
            this.load.audio('BlockHit', './Assets/Audio/Block Hit.wav');
            this.load.audio('BGM', './Assets/Audio/BGM.mp3');
            this.load.audio('BossBGM', './Assets/Audio/BossBGM.mp3');
        }

		create()
		{
            this.game.scale.startFullScreen();
            var tween = this.add.tween(this.preloadBar).to({ alpha: 0 }, 1000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.startMainMenu, this);
            TimerListener.Instance.InitTimer(this.game);
            AudioPlayer.Instance.Init(this.game);
        }

     

        startMainMenu() {

            this.game.state.start('MainMenu', true, false);

        }

    }

}