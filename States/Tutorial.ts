﻿module MarioSis
{
	export class Tutorial extends Phaser.State
	{
		mLevel: number;
		mLevelText: Phaser.Text;
		mObjectiveText: Phaser.Text;
		mMonster: Phaser.Text;

		init(level: number)
		{
			
		}

		create()
		{
			var video = this.game.add.video('Intro');

			//  See the docs for the full parameters
			//  But it goes x, y, anchor x, anchor y, scale x, scale y
			var sprite = video.addToWorld(this.game.world.centerX, this.game.world.centerY, 0.5, 0.5, 2, 2);

			//  true = loop
			video.play(true);

		}

		update(): void
		{
		}

		

		startGame()
		{
			this.game.state.start('GameWorld', true, false, this.mLevel);
		}

	}

}